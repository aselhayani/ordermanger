﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Cars
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim PlatNBLabel As System.Windows.Forms.Label
        Dim CarDriverLabel As System.Windows.Forms.Label
        Dim CarCapacityLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Cars))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.PlatNBTextBox = New System.Windows.Forms.TextBox()
        Me.CarDriverComboBox = New System.Windows.Forms.ComboBox()
        Me.CarCapacityTextBox = New System.Windows.Forms.TextBox()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.SaveButton = New System.Windows.Forms.ToolStripButton()
        Me.DeleteButton = New System.Windows.Forms.ToolStripButton()
        Me.NewButton = New System.Windows.Forms.ToolStripButton()
        PlatNBLabel = New System.Windows.Forms.Label()
        CarDriverLabel = New System.Windows.Forms.Label()
        CarCapacityLabel = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PlatNBLabel
        '
        PlatNBLabel.AutoSize = True
        PlatNBLabel.Location = New System.Drawing.Point(451, 32)
        PlatNBLabel.Name = "PlatNBLabel"
        PlatNBLabel.Size = New System.Drawing.Size(45, 15)
        PlatNBLabel.TabIndex = 2
        PlatNBLabel.Text = "رقم اللوحه"
        '
        'CarDriverLabel
        '
        CarDriverLabel.AutoSize = True
        CarDriverLabel.Location = New System.Drawing.Point(449, 66)
        CarDriverLabel.Name = "CarDriverLabel"
        CarDriverLabel.Size = New System.Drawing.Size(45, 15)
        CarDriverLabel.TabIndex = 4
        CarDriverLabel.Text = "اسم السائق"
        '
        'CarCapacityLabel
        '
        CarCapacityLabel.AutoSize = True
        CarCapacityLabel.Location = New System.Drawing.Point(442, 97)
        CarCapacityLabel.Name = "CarCapacityLabel"
        CarCapacityLabel.Size = New System.Drawing.Size(55, 15)
        CarCapacityLabel.TabIndex = 6
        CarCapacityLabel.Text = "اقصي حموله"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(14, 32)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(280, 255)
        Me.DataGridView1.TabIndex = 0
        '
        'PlatNBTextBox
        '
        Me.PlatNBTextBox.Location = New System.Drawing.Point(301, 32)
        Me.PlatNBTextBox.Name = "PlatNBTextBox"
        Me.PlatNBTextBox.Size = New System.Drawing.Size(140, 21)
        Me.PlatNBTextBox.TabIndex = 3
        '
        'CarDriverComboBox
        '
        Me.CarDriverComboBox.FormattingEnabled = True
        Me.CarDriverComboBox.Location = New System.Drawing.Point(301, 62)
        Me.CarDriverComboBox.Name = "CarDriverComboBox"
        Me.CarDriverComboBox.Size = New System.Drawing.Size(140, 23)
        Me.CarDriverComboBox.TabIndex = 5
        '
        'CarCapacityTextBox
        '
        Me.CarCapacityTextBox.Location = New System.Drawing.Point(301, 93)
        Me.CarCapacityTextBox.Name = "CarCapacityTextBox"
        Me.CarCapacityTextBox.Size = New System.Drawing.Size(140, 21)
        Me.CarCapacityTextBox.TabIndex = 7
        Me.CarCapacityTextBox.Text = "0"
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaveButton, Me.DeleteButton, Me.NewButton})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ToolStrip1.Size = New System.Drawing.Size(524, 25)
        Me.ToolStrip1.TabIndex = 8
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'SaveButton
        '
        Me.SaveButton.Image = Global.OrderManger.My.Resources.Resources.floppy_disk_save_button_icon_65887
        Me.SaveButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(50, 22)
        Me.SaveButton.Text = "حفظ"
        '
        'DeleteButton
        '
        Me.DeleteButton.Image = Global.OrderManger.My.Resources.Resources._627249_delete3_512
        Me.DeleteButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.DeleteButton.Name = "DeleteButton"
        Me.DeleteButton.Size = New System.Drawing.Size(52, 22)
        Me.DeleteButton.Text = "حذف"
        '
        'NewButton
        '
        Me.NewButton.Image = Global.OrderManger.My.Resources.Resources.orange_plus_sign_icon_32197
        Me.NewButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.NewButton.Name = "NewButton"
        Me.NewButton.Size = New System.Drawing.Size(50, 22)
        Me.NewButton.Text = "جديد"
        '
        'Cars
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 310)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(PlatNBLabel)
        Me.Controls.Add(Me.PlatNBTextBox)
        Me.Controls.Add(CarDriverLabel)
        Me.Controls.Add(Me.CarDriverComboBox)
        Me.Controls.Add(CarCapacityLabel)
        Me.Controls.Add(Me.CarCapacityTextBox)
        Me.Controls.Add(Me.DataGridView1)
        Me.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Cars"
        Me.Opacity = 0R
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Text = "السيارات"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents OrderMangerDBDataSet As OrderMangerDBDataSet
    Friend WithEvents CarsBindingSource As BindingSource
    Friend WithEvents CarsTableAdapter As OrderMangerDBDataSetTableAdapters.CarsTableAdapter
    Friend WithEvents TableAdapterManager As OrderMangerDBDataSetTableAdapters.TableAdapterManager
    Friend WithEvents PlatNBTextBox As TextBox
    Friend WithEvents CarDriverComboBox As ComboBox
    Friend WithEvents CarCapacityTextBox As TextBox
    Friend WithEvents ToolStrip1 As ToolStrip
    Friend WithEvents SaveButton As ToolStripButton
    Friend WithEvents DeleteButton As ToolStripButton
    Friend WithEvents NewButton As ToolStripButton
End Class
