﻿Imports System.Drawing.Printing

Public Class PrintSetting

    Private Sub PrintSetting_Load(sender As Object, e As EventArgs) Handles MyBase.Load




        Dim i As Integer
        Dim pkInstalledPrinters As String
        For i = 0 To PrinterSettings.InstalledPrinters.Count - 1
            pkInstalledPrinters = PrinterSettings.InstalledPrinters.Item(i)
            ComboBox3.Items.Add(pkInstalledPrinters)
        Next
        ComboBox3.Text = My.Settings.DefultPrinter



        Dim PaperDT As New DataTable
        PaperDT.Columns.Add("PaperName")
        PaperDT.Columns.Add("PaperNumber")


        Dim printDoc As New PrintDocument
        Dim pkSize As PaperSize
        For i = 0 To printDoc.PrinterSettings.PaperSizes.Count - 1
            pkSize = printDoc.PrinterSettings.PaperSizes.Item(i)
            PaperDT.Rows.Add()
            PaperDT.Rows(PaperDT.Rows.Count - 1).Item("PaperName") = pkSize.Kind
            PaperDT.Rows(PaperDT.Rows.Count - 1).Item("PaperNumber") = pkSize.RawKind

        Next
        ComboBox1.DataSource = PaperDT
        ComboBox1.DisplayMember = "PaperName"
        ComboBox1.ValueMember = "PaperNumber"

        ComboBox1.SelectedValue = CType(My.Settings.PaperSize, Integer)



        SpinEdit1.EditValue = My.Settings.MTop
        SpinEdit2.EditValue = My.Settings.MBottom
        SpinEdit3.EditValue = My.Settings.MRight
        SpinEdit4.EditValue = My.Settings.MLeft
        CheckButton1.Checked = My.Settings.LandScape

    End Sub


    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        My.Settings.MTop = SpinEdit1.EditValue
        My.Settings.MBottom = SpinEdit2.EditValue
        My.Settings.MRight = SpinEdit3.EditValue
        My.Settings.MLeft = SpinEdit4.EditValue
        My.Settings.LandScape = CheckButton1.Checked
        My.Settings.PaperSize = ComboBox1.SelectedValue
        My.Settings.DefultPrinter = ComboBox3.Text
        My.Settings.Save()


    End Sub
End Class