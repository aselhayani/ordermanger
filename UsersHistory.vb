﻿Imports System.Data.SqlClient
Imports DevExpress.Utils

Public Class UsersHistory
    Private Sub UsersHistory_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim UserHistoryDA As New SqlDataAdapter("Select * from UserHistory order by ActionCode ", SQlManeCon)
        Dim UserHistoryDT As New DataTable
        UserHistoryDT.Clear()
        UserHistoryDA.Fill(UserHistoryDT)

        GridControl1.BeginUpdate()
        Try
            GridView1.Columns.Clear()
            GridControl1.DataSource = Nothing
            GridControl1.DataSource = UserHistoryDT

        Finally
            GridControl1.EndUpdate()

        End Try

        GridView1.Columns("UserName").Caption = "المستخدم"
        GridView1.Columns("UserScreen").Caption = "الشاشه"
        GridView1.Columns("UserAction").Caption = "الاجراء"

        GridView1.Columns("ActionCode").Caption = "الكود"
        GridView1.Columns("ActionPart").Caption = "الطرف"
        GridView1.Columns("ActionDate").Caption = "الوقت"

        GridView1.Columns("ActionDate").DisplayFormat.FormatType = FormatType.Custom
        GridView1.Columns("ActionDate").DisplayFormat.FormatString = "yyyy-MM-dd hh:mm:ss tt "



    End Sub



End Class