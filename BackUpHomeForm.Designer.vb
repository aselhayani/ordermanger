﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BackUpHomeForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TileItemElement1 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(BackUpHomeForm))
        Dim TileItemElement2 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement3 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement4 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement5 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement6 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement7 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement8 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement9 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement10 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement11 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement12 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement13 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Dim TileItemElement14 As DevExpress.XtraEditors.TileItemElement = New DevExpress.XtraEditors.TileItemElement()
        Me.TileControl1 = New DevExpress.XtraEditors.TileControl()
        Me.SettingTileGroup = New DevExpress.XtraEditors.TileGroup()
        Me.CatsTile = New DevExpress.XtraEditors.TileItem()
        Me.driversTile = New DevExpress.XtraEditors.TileItem()
        Me.TileItem1 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem2 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem7 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem11 = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup2 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem3 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem4 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem8 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem6 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem5 = New DevExpress.XtraEditors.TileItem()
        Me.التقارير = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem12 = New DevExpress.XtraEditors.TileItem()
        Me.TileGroup4 = New DevExpress.XtraEditors.TileGroup()
        Me.TileItem9 = New DevExpress.XtraEditors.TileItem()
        Me.TileItem10 = New DevExpress.XtraEditors.TileItem()
        Me.SuspendLayout()
        '
        'TileControl1
        '
        Me.TileControl1.AllowItemHover = True
        Me.TileControl1.AppearanceItem.Hovered.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TileControl1.AppearanceItem.Hovered.Options.UseFont = True
        Me.TileControl1.AppearanceItem.Normal.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TileControl1.AppearanceItem.Normal.Options.UseFont = True
        Me.TileControl1.AppearanceItem.Pressed.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TileControl1.AppearanceItem.Pressed.Options.UseFont = True
        Me.TileControl1.AppearanceItem.Selected.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Bold)
        Me.TileControl1.AppearanceItem.Selected.Options.UseFont = True
        Me.TileControl1.AppearanceText.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TileControl1.AppearanceText.Options.UseFont = True
        Me.TileControl1.BackColor = System.Drawing.Color.Gainsboro
        Me.TileControl1.Cursor = System.Windows.Forms.Cursors.Default
        Me.TileControl1.Groups.Add(Me.SettingTileGroup)
        Me.TileControl1.Groups.Add(Me.TileGroup2)
        Me.TileControl1.Groups.Add(Me.التقارير)
        Me.TileControl1.Groups.Add(Me.TileGroup4)
        Me.TileControl1.ItemPadding = New System.Windows.Forms.Padding(6)
        Me.TileControl1.ItemSize = 100
        Me.TileControl1.Location = New System.Drawing.Point(4, -1)
        Me.TileControl1.MaxId = 18
        Me.TileControl1.Name = "TileControl1"
        Me.TileControl1.Padding = New System.Windows.Forms.Padding(15)
        Me.TileControl1.ScrollMode = DevExpress.XtraEditors.TileControlScrollMode.ScrollBar
        Me.TileControl1.ShowGroupText = True
        Me.TileControl1.Size = New System.Drawing.Size(276, 263)
        Me.TileControl1.TabIndex = 1
        Me.TileControl1.Text = "D"
        Me.TileControl1.Visible = False
        '
        'SettingTileGroup
        '
        Me.SettingTileGroup.Items.Add(Me.CatsTile)
        Me.SettingTileGroup.Items.Add(Me.driversTile)
        Me.SettingTileGroup.Items.Add(Me.TileItem1)
        Me.SettingTileGroup.Items.Add(Me.TileItem2)
        Me.SettingTileGroup.Items.Add(Me.TileItem7)
        Me.SettingTileGroup.Items.Add(Me.TileItem11)
        Me.SettingTileGroup.Name = "SettingTileGroup"
        Me.SettingTileGroup.Text = "الاعدادات"
        '
        'CatsTile
        '
        Me.CatsTile.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.CatsTile.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement1.Image = CType(resources.GetObject("TileItemElement1.Image"), System.Drawing.Image)
        TileItemElement1.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside
        TileItemElement1.Text = "السيارات"
        TileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight
        Me.CatsTile.Elements.Add(TileItemElement1)
        Me.CatsTile.Id = 2
        Me.CatsTile.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.CatsTile.Name = "CatsTile"
        '
        'driversTile
        '
        Me.driversTile.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.driversTile.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement2.Image = CType(resources.GetObject("TileItemElement2.Image"), System.Drawing.Image)
        TileItemElement2.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside
        TileItemElement2.Text = "السائقون"
        TileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight
        Me.driversTile.Elements.Add(TileItemElement2)
        Me.driversTile.Id = 3
        Me.driversTile.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.driversTile.Name = "driversTile"
        Me.driversTile.TextShowMode = DevExpress.XtraEditors.TileItemContentShowMode.Hover
        '
        'TileItem1
        '
        Me.TileItem1.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem1.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement3.Image = CType(resources.GetObject("TileItemElement3.Image"), System.Drawing.Image)
        TileItemElement3.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside
        TileItemElement3.Text = "الاصناف"
        TileItemElement3.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight
        Me.TileItem1.Elements.Add(TileItemElement3)
        Me.TileItem1.Id = 4
        Me.TileItem1.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItem1.Name = "TileItem1"
        Me.TileItem1.TextShowMode = DevExpress.XtraEditors.TileItemContentShowMode.Hover
        '
        'TileItem2
        '
        Me.TileItem2.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem2.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement4.Image = CType(resources.GetObject("TileItemElement4.Image"), System.Drawing.Image)
        TileItemElement4.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside
        TileItemElement4.Text = "خطوط الانتاج"
        TileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight
        Me.TileItem2.Elements.Add(TileItemElement4)
        Me.TileItem2.Id = 5
        Me.TileItem2.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItem2.Name = "TileItem2"
        Me.TileItem2.TextShowMode = DevExpress.XtraEditors.TileItemContentShowMode.Hover
        '
        'TileItem7
        '
        Me.TileItem7.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem7.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement5.Text = "الفئات"
        Me.TileItem7.Elements.Add(TileItemElement5)
        Me.TileItem7.Id = 13
        Me.TileItem7.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItem7.Name = "TileItem7"
        '
        'TileItem11
        '
        Me.TileItem11.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem11.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement6.Text = "اعدادات الطباعه "
        Me.TileItem11.Elements.Add(TileItemElement6)
        Me.TileItem11.Id = 16
        Me.TileItem11.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItem11.Name = "TileItem11"
        '
        'TileGroup2
        '
        Me.TileGroup2.Items.Add(Me.TileItem3)
        Me.TileGroup2.Items.Add(Me.TileItem4)
        Me.TileGroup2.Items.Add(Me.TileItem8)
        Me.TileGroup2.Items.Add(Me.TileItem6)
        Me.TileGroup2.Items.Add(Me.TileItem5)
        Me.TileGroup2.Name = "TileGroup2"
        Me.TileGroup2.Text = "الطلبيات"
        '
        'TileItem3
        '
        Me.TileItem3.AppearanceItem.Normal.BackColor = System.Drawing.Color.Green
        Me.TileItem3.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem3.AppearanceItem.Normal.Options.UseBackColor = True
        Me.TileItem3.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement7.Image = CType(resources.GetObject("TileItemElement7.Image"), System.Drawing.Image)
        TileItemElement7.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside
        TileItemElement7.Text = "طلبيه جديده"
        TileItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight
        Me.TileItem3.Elements.Add(TileItemElement7)
        Me.TileItem3.Id = 6
        Me.TileItem3.ItemSize = DevExpress.XtraEditors.TileItemSize.Large
        Me.TileItem3.Name = "TileItem3"
        '
        'TileItem4
        '
        Me.TileItem4.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem4.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement8.Image = CType(resources.GetObject("TileItemElement8.Image"), System.Drawing.Image)
        TileItemElement8.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside
        TileItemElement8.Text = "الطلبيات"
        TileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight
        Me.TileItem4.Elements.Add(TileItemElement8)
        Me.TileItem4.Id = 7
        Me.TileItem4.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem4.Name = "TileItem4"
        '
        'TileItem8
        '
        Me.TileItem8.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem8.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement9.Image = Global.OrderManger.My.Resources.Resources._112_Tick_Blue
        TileItemElement9.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopCenter
        TileItemElement9.ImageToTextAlignment = DevExpress.XtraEditors.TileControlImageToTextAlignment.Left
        TileItemElement9.Text = "الطلبيات التي تم تسليمها "
        Me.TileItem8.Elements.Add(TileItemElement9)
        Me.TileItem8.Id = 12
        Me.TileItem8.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem8.Name = "TileItem8"
        '
        'TileItem6
        '
        Me.TileItem6.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem6.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement10.Image = CType(resources.GetObject("TileItemElement10.Image"), System.Drawing.Image)
        TileItemElement10.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside
        TileItemElement10.Text = "حموله السيارات"
        TileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight
        Me.TileItem6.Elements.Add(TileItemElement10)
        Me.TileItem6.Id = 9
        Me.TileItem6.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem6.Name = "TileItem6"
        '
        'TileItem5
        '
        Me.TileItem5.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem5.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement11.Image = CType(resources.GetObject("TileItemElement11.Image"), System.Drawing.Image)
        TileItemElement11.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomInside
        TileItemElement11.Text = "جدول انتاج اليوم "
        TileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight
        Me.TileItem5.Elements.Add(TileItemElement11)
        Me.TileItem5.Id = 8
        Me.TileItem5.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium
        Me.TileItem5.Name = "TileItem5"
        '
        'التقارير
        '
        Me.التقارير.Items.Add(Me.TileItem12)
        Me.التقارير.Name = "التقارير"
        Me.التقارير.Tag = "التقارير"
        Me.التقارير.Text = "التقارير"
        '
        'TileItem12
        '
        Me.TileItem12.AppearanceItem.Normal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.TileItem12.AppearanceItem.Normal.Options.UseFont = True
        TileItemElement12.Text = "TileItem12"
        Me.TileItem12.Elements.Add(TileItemElement12)
        Me.TileItem12.Id = 17
        Me.TileItem12.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem12.Name = "TileItem12"
        '
        'TileGroup4
        '
        Me.TileGroup4.Items.Add(Me.TileItem9)
        Me.TileGroup4.Items.Add(Me.TileItem10)
        Me.TileGroup4.Name = "TileGroup4"
        Me.TileGroup4.Text = "أداره المستخدمين"
        '
        'TileItem9
        '
        TileItemElement13.Text = "المستخدمين"
        Me.TileItem9.Elements.Add(TileItemElement13)
        Me.TileItem9.Id = 14
        Me.TileItem9.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem9.Name = "TileItem9"
        '
        'TileItem10
        '
        TileItemElement14.Text = "السجل"
        Me.TileItem10.Elements.Add(TileItemElement14)
        Me.TileItem10.Id = 15
        Me.TileItem10.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide
        Me.TileItem10.Name = "TileItem10"
        '
        'BackUpHomeForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.TileControl1)
        Me.Name = "BackUpHomeForm"
        Me.Text = "BackUpHomeForm"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TileControl1 As DevExpress.XtraEditors.TileControl
    Friend WithEvents SettingTileGroup As DevExpress.XtraEditors.TileGroup
    Friend WithEvents CatsTile As DevExpress.XtraEditors.TileItem
    Friend WithEvents driversTile As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem1 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem2 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem7 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem11 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup2 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem3 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem4 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem8 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem6 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem5 As DevExpress.XtraEditors.TileItem
    Friend WithEvents التقارير As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem12 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileGroup4 As DevExpress.XtraEditors.TileGroup
    Friend WithEvents TileItem9 As DevExpress.XtraEditors.TileItem
    Friend WithEvents TileItem10 As DevExpress.XtraEditors.TileItem
End Class
