﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Drivers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DriverNameLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Drivers))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.DriverBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorAddNewItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.DriverBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.DriverNameTextBox = New System.Windows.Forms.TextBox()
        DriverNameLabel = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DriverBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DriverBindingNavigator.SuspendLayout()
        Me.SuspendLayout()
        '
        'DriverNameLabel
        '
        DriverNameLabel.AutoSize = True
        DriverNameLabel.Location = New System.Drawing.Point(211, 36)
        DriverNameLabel.Name = "DriverNameLabel"
        DriverNameLabel.Size = New System.Drawing.Size(58, 13)
        DriverNameLabel.TabIndex = 2
        DriverNameLabel.Text = "اسم السائق"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(12, 59)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(256, 190)
        Me.DataGridView1.TabIndex = 0
        '
        'DriverBindingNavigator
        '
        Me.DriverBindingNavigator.AddNewItem = Me.BindingNavigatorAddNewItem
        Me.DriverBindingNavigator.CountItem = Nothing
        Me.DriverBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.DriverBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorAddNewItem, Me.BindingNavigatorDeleteItem, Me.DriverBindingNavigatorSaveItem})
        Me.DriverBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.DriverBindingNavigator.MoveFirstItem = Nothing
        Me.DriverBindingNavigator.MoveLastItem = Nothing
        Me.DriverBindingNavigator.MoveNextItem = Nothing
        Me.DriverBindingNavigator.MovePreviousItem = Nothing
        Me.DriverBindingNavigator.Name = "DriverBindingNavigator"
        Me.DriverBindingNavigator.PositionItem = Nothing
        Me.DriverBindingNavigator.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DriverBindingNavigator.Size = New System.Drawing.Size(292, 25)
        Me.DriverBindingNavigator.TabIndex = 1
        Me.DriverBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorAddNewItem
        '
        Me.BindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorAddNewItem.Image = CType(resources.GetObject("BindingNavigatorAddNewItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorAddNewItem.Name = "BindingNavigatorAddNewItem"
        Me.BindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorAddNewItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorAddNewItem.Text = "Add new"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorDeleteItem.Text = "Delete"
        '
        'DriverBindingNavigatorSaveItem
        '
        Me.DriverBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.DriverBindingNavigatorSaveItem.Image = CType(resources.GetObject("DriverBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.DriverBindingNavigatorSaveItem.Name = "DriverBindingNavigatorSaveItem"
        Me.DriverBindingNavigatorSaveItem.Size = New System.Drawing.Size(23, 22)
        Me.DriverBindingNavigatorSaveItem.Text = "Save Data"
        '
        'DriverNameTextBox
        '
        Me.DriverNameTextBox.Location = New System.Drawing.Point(12, 33)
        Me.DriverNameTextBox.Name = "DriverNameTextBox"
        Me.DriverNameTextBox.Size = New System.Drawing.Size(193, 20)
        Me.DriverNameTextBox.TabIndex = 3
        '
        'Drivers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 265)
        Me.Controls.Add(DriverNameLabel)
        Me.Controls.Add(Me.DriverNameTextBox)
        Me.Controls.Add(Me.DriverBindingNavigator)
        Me.Controls.Add(Me.DataGridView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Drivers"
        Me.Opacity = 0R
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Text = "السائقون"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DriverBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DriverBindingNavigator.ResumeLayout(False)
        Me.DriverBindingNavigator.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents DriverBindingNavigator As BindingNavigator
    Friend WithEvents BindingNavigatorAddNewItem As ToolStripButton
    Friend WithEvents BindingNavigatorDeleteItem As ToolStripButton
    Friend WithEvents DriverBindingNavigatorSaveItem As ToolStripButton
    Friend WithEvents DriverNameTextBox As TextBox
End Class
