﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class NewOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DelivaryCarLabel As System.Windows.Forms.Label
        Dim DelivaryDriverLabel As System.Windows.Forms.Label
        Dim ImageIndicatorState1 As DevExpress.XtraGauges.Core.Model.ImageIndicatorState = New DevExpress.XtraGauges.Core.Model.ImageIndicatorState()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(NewOrder))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.OrderIDTextBox = New System.Windows.Forms.TextBox()
        Me.OrderClintComboBox = New System.Windows.Forms.ComboBox()
        Me.OrderProductComboBox = New System.Windows.Forms.ComboBox()
        Me.OrderQuntityTextBox = New System.Windows.Forms.TextBox()
        Me.ProductLineTextBox = New System.Windows.Forms.TextBox()
        Me.DelivaryCarComboBox = New System.Windows.Forms.ComboBox()
        Me.DelivaryDriverComboBox = New System.Windows.Forms.ComboBox()
        Me.OrderDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.DeliveryDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ProductionDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GaugeControl1 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.circularGauge1 = New DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge()
        Me.StateImageIndicatorComponent1 = New DevExpress.XtraGauges.Win.Base.StateImageIndicatorComponent()
        Me.ArcScaleComponent1 = New DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ArcScaleRangeBarComponent1 = New DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleRangeBarComponent()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupCompoBox = New System.Windows.Forms.ComboBox()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        DelivaryCarLabel = New System.Windows.Forms.Label()
        DelivaryDriverLabel = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.circularGauge1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateImageIndicatorComponent1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ArcScaleComponent1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ArcScaleRangeBarComponent1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        Me.SuspendLayout()
        '
        'DelivaryCarLabel
        '
        DelivaryCarLabel.AutoSize = True
        DelivaryCarLabel.Location = New System.Drawing.Point(458, 29)
        DelivaryCarLabel.Name = "DelivaryCarLabel"
        DelivaryCarLabel.Size = New System.Drawing.Size(41, 13)
        DelivaryCarLabel.TabIndex = 12
        DelivaryCarLabel.Text = "السياره"
        '
        'DelivaryDriverLabel
        '
        DelivaryDriverLabel.AutoSize = True
        DelivaryDriverLabel.Location = New System.Drawing.Point(254, 26)
        DelivaryDriverLabel.Name = "DelivaryDriverLabel"
        DelivaryDriverLabel.Size = New System.Drawing.Size(41, 13)
        DelivaryDriverLabel.TabIndex = 14
        DelivaryDriverLabel.Text = "السائق"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(6, 107)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(935, 165)
        Me.DataGridView1.TabIndex = 100
        '
        'OrderIDTextBox
        '
        Me.OrderIDTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OrderIDTextBox.Enabled = False
        Me.OrderIDTextBox.Location = New System.Drawing.Point(2, 20)
        Me.OrderIDTextBox.Name = "OrderIDTextBox"
        Me.OrderIDTextBox.Size = New System.Drawing.Size(73, 20)
        Me.OrderIDTextBox.TabIndex = 3
        '
        'OrderClintComboBox
        '
        Me.OrderClintComboBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OrderClintComboBox.FormattingEnabled = True
        Me.OrderClintComboBox.Location = New System.Drawing.Point(2, 20)
        Me.OrderClintComboBox.Name = "OrderClintComboBox"
        Me.OrderClintComboBox.Size = New System.Drawing.Size(196, 21)
        Me.OrderClintComboBox.TabIndex = 2
        '
        'OrderProductComboBox
        '
        Me.OrderProductComboBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OrderProductComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.OrderProductComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.OrderProductComboBox.FormattingEnabled = True
        Me.OrderProductComboBox.Location = New System.Drawing.Point(706, 22)
        Me.OrderProductComboBox.Name = "OrderProductComboBox"
        Me.OrderProductComboBox.Size = New System.Drawing.Size(227, 21)
        Me.OrderProductComboBox.TabIndex = 3
        '
        'OrderQuntityTextBox
        '
        Me.OrderQuntityTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OrderQuntityTextBox.Location = New System.Drawing.Point(592, 22)
        Me.OrderQuntityTextBox.Name = "OrderQuntityTextBox"
        Me.OrderQuntityTextBox.Size = New System.Drawing.Size(108, 20)
        Me.OrderQuntityTextBox.TabIndex = 4
        '
        'ProductLineTextBox
        '
        Me.ProductLineTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProductLineTextBox.Enabled = False
        Me.ProductLineTextBox.Location = New System.Drawing.Point(706, 305)
        Me.ProductLineTextBox.Name = "ProductLineTextBox"
        Me.ProductLineTextBox.Size = New System.Drawing.Size(60, 20)
        Me.ProductLineTextBox.TabIndex = 11
        Me.ProductLineTextBox.Visible = False
        '
        'DelivaryCarComboBox
        '
        Me.DelivaryCarComboBox.FormattingEnabled = True
        Me.DelivaryCarComboBox.Location = New System.Drawing.Point(322, 26)
        Me.DelivaryCarComboBox.Name = "DelivaryCarComboBox"
        Me.DelivaryCarComboBox.Size = New System.Drawing.Size(130, 21)
        Me.DelivaryCarComboBox.TabIndex = 8
        '
        'DelivaryDriverComboBox
        '
        Me.DelivaryDriverComboBox.FormattingEnabled = True
        Me.DelivaryDriverComboBox.Location = New System.Drawing.Point(117, 23)
        Me.DelivaryDriverComboBox.Name = "DelivaryDriverComboBox"
        Me.DelivaryDriverComboBox.Size = New System.Drawing.Size(124, 21)
        Me.DelivaryDriverComboBox.TabIndex = 15
        '
        'OrderDateDateTimePicker
        '
        Me.OrderDateDateTimePicker.CustomFormat = "yyyy/MM/dd  hh:mm"
        Me.OrderDateDateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill
        Me.OrderDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.OrderDateDateTimePicker.Location = New System.Drawing.Point(2, 20)
        Me.OrderDateDateTimePicker.Name = "OrderDateDateTimePicker"
        Me.OrderDateDateTimePicker.Size = New System.Drawing.Size(119, 20)
        Me.OrderDateDateTimePicker.TabIndex = 17
        '
        'DeliveryDateDateTimePicker
        '
        Me.DeliveryDateDateTimePicker.CustomFormat = "yyyy/MM/dd"
        Me.DeliveryDateDateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DeliveryDateDateTimePicker.Enabled = False
        Me.DeliveryDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DeliveryDateDateTimePicker.Location = New System.Drawing.Point(2, 20)
        Me.DeliveryDateDateTimePicker.Name = "DeliveryDateDateTimePicker"
        Me.DeliveryDateDateTimePicker.Size = New System.Drawing.Size(107, 20)
        Me.DeliveryDateDateTimePicker.TabIndex = 7
        '
        'ProductionDateDateTimePicker
        '
        Me.ProductionDateDateTimePicker.CustomFormat = "yyyy/MM/dd"
        Me.ProductionDateDateTimePicker.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ProductionDateDateTimePicker.Enabled = False
        Me.ProductionDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.ProductionDateDateTimePicker.Location = New System.Drawing.Point(2, 20)
        Me.ProductionDateDateTimePicker.Name = "ProductionDateDateTimePicker"
        Me.ProductionDateDateTimePicker.Size = New System.Drawing.Size(118, 20)
        Me.ProductionDateDateTimePicker.TabIndex = 21
        Me.ProductionDateDateTimePicker.Value = New Date(2017, 10, 11, 0, 0, 0, 0)
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(713, 329)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(54, 20)
        Me.TextBox1.TabIndex = 22
        Me.TextBox1.Visible = False
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(500, 22)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 21)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "اضافه"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(207, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "ح.س خلال اليوم"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(138, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(13, 13)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "0"
        '
        'TextBox2
        '
        Me.TextBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox2.Location = New System.Drawing.Point(720, 354)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(47, 20)
        Me.TextBox2.TabIndex = 27
        Me.TextBox2.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(422, 64)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "حموله الطلبيه"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(343, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(13, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(179, 92)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "اجمالي حموله السياره"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(138, 92)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(13, 13)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(383, 92)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(107, 13)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "اقصي حموله للسياره "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(343, 92)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(13, 13)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "0"
        '
        'GaugeControl1
        '
        Me.GaugeControl1.ColorScheme.TargetElements = CType((DevExpress.XtraGauges.Core.Base.TargetElement.RangeBar Or DevExpress.XtraGauges.Core.Base.TargetElement.Label), DevExpress.XtraGauges.Core.Base.TargetElement)
        Me.GaugeControl1.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.circularGauge1})
        Me.GaugeControl1.Location = New System.Drawing.Point(16, 23)
        Me.GaugeControl1.Name = "GaugeControl1"
        Me.GaugeControl1.Size = New System.Drawing.Size(95, 85)
        Me.GaugeControl1.TabIndex = 30
        '
        'circularGauge1
        '
        Me.circularGauge1.Bounds = New System.Drawing.Rectangle(6, 6, 83, 73)
        Me.circularGauge1.ImageIndicators.AddRange(New DevExpress.XtraGauges.Win.Base.StateImageIndicatorComponent() {Me.StateImageIndicatorComponent1})
        Me.circularGauge1.Name = "circularGauge1"
        Me.circularGauge1.RangeBars.AddRange(New DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleRangeBarComponent() {Me.ArcScaleRangeBarComponent1})
        Me.circularGauge1.Scales.AddRange(New DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent() {Me.ArcScaleComponent1})
        '
        'StateImageIndicatorComponent1
        '
        Me.StateImageIndicatorComponent1.ImageLayoutMode = DevExpress.XtraGauges.Core.Drawing.ImageLayoutMode.BottomRight
        ImageIndicatorState1.Name = "State1"
        Me.StateImageIndicatorComponent1.ImageStateCollection.AddRange(New DevExpress.XtraGauges.Core.Model.ImageIndicatorState() {ImageIndicatorState1})
        Me.StateImageIndicatorComponent1.IndicatorScale = Me.ArcScaleComponent1
        Me.StateImageIndicatorComponent1.Name = "circularGauge1_StateImageIndicator1"
        Me.StateImageIndicatorComponent1.StateImages = Me.ImageList1
        Me.StateImageIndicatorComponent1.StateIndex = 1
        '
        'ArcScaleComponent1
        '
        Me.ArcScaleComponent1.AppearanceMajorTickmark.BorderBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White")
        Me.ArcScaleComponent1.AppearanceMajorTickmark.ContentBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White")
        Me.ArcScaleComponent1.AppearanceMinorTickmark.BorderBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White")
        Me.ArcScaleComponent1.AppearanceMinorTickmark.ContentBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White")
        Me.ArcScaleComponent1.AppearanceTickmarkText.Font = New System.Drawing.Font("Tahoma", 8.5!)
        Me.ArcScaleComponent1.AppearanceTickmarkText.TextBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#484E5A")
        Me.ArcScaleComponent1.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(125.0!, 125.0!)
        Me.ArcScaleComponent1.EndAngle = 90.0!
        Me.ArcScaleComponent1.MajorTickCount = 0
        Me.ArcScaleComponent1.MajorTickmark.FormatString = "{0:F0}"
        Me.ArcScaleComponent1.MajorTickmark.ShapeOffset = -14.0!
        Me.ArcScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style16_1
        Me.ArcScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight
        Me.ArcScaleComponent1.MaxValue = 100.0!
        Me.ArcScaleComponent1.MinorTickCount = 0
        Me.ArcScaleComponent1.MinorTickmark.ShapeOffset = -7.0!
        Me.ArcScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style16_2
        Me.ArcScaleComponent1.Name = "scale1"
        Me.ArcScaleComponent1.StartAngle = -270.0!
        Me.ArcScaleComponent1.Value = 20.0!
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "78a4211079921cd8604d573ea33f48c3.png")
        Me.ImageList1.Images.SetKeyName(1, "2000px-Warning_sign_font_awesome-red.svg.png")
        '
        'ArcScaleRangeBarComponent1
        '
        Me.ArcScaleRangeBarComponent1.AnchorValue = 1.0!
        Me.ArcScaleRangeBarComponent1.ArcScale = Me.ArcScaleComponent1
        Me.ArcScaleRangeBarComponent1.EndOffset = 5.0!
        Me.ArcScaleRangeBarComponent1.Name = "circularGauge1_RangeBar2"
        Me.ArcScaleRangeBarComponent1.ShowBackground = True
        Me.ArcScaleRangeBarComponent1.StartOffset = 65.0!
        Me.ArcScaleRangeBarComponent1.Value = 10.0!
        Me.ArcScaleRangeBarComponent1.ZOrder = -10
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(322, 60)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(174, 20)
        Me.TextBox3.TabIndex = 26
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(118, 88)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(174, 20)
        Me.TextBox6.TabIndex = 29
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(322, 88)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(174, 20)
        Me.TextBox5.TabIndex = 28
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(117, 60)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(174, 20)
        Me.TextBox4.TabIndex = 27
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Dock = System.Windows.Forms.DockStyle.Left
        Me.SimpleButton1.ImageOptions.Image = Global.OrderManger.My.Resources.Resources.delete_16x16
        Me.SimpleButton1.Location = New System.Drawing.Point(2, 20)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(25, 25)
        Me.SimpleButton1.TabIndex = 9
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.BackColor = System.Drawing.Color.DarkSeaGreen
        Me.Button3.Location = New System.Drawing.Point(772, 9)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(80, 41)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "جديد"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button4.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.Button4.Location = New System.Drawing.Point(857, 9)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(84, 41)
        Me.Button4.TabIndex = 9
        Me.Button4.Text = "حفظ"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'GroupCompoBox
        '
        Me.GroupCompoBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupCompoBox.FormattingEnabled = True
        Me.GroupCompoBox.Location = New System.Drawing.Point(2, 20)
        Me.GroupCompoBox.Name = "GroupCompoBox"
        Me.GroupCompoBox.Size = New System.Drawing.Size(113, 21)
        Me.GroupCompoBox.TabIndex = 1
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.OrderClintComboBox)
        Me.GroupControl1.Location = New System.Drawing.Point(363, 9)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(200, 41)
        Me.GroupControl1.TabIndex = 22
        Me.GroupControl1.Text = "اسم العميل"
        '
        'GroupControl2
        '
        Me.GroupControl2.Controls.Add(Me.OrderDateDateTimePicker)
        Me.GroupControl2.Location = New System.Drawing.Point(236, 9)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(123, 41)
        Me.GroupControl2.TabIndex = 23
        Me.GroupControl2.Text = "تاريخ الطلبيه"
        '
        'GroupControl3
        '
        Me.GroupControl3.Controls.Add(Me.DeliveryDateDateTimePicker)
        Me.GroupControl3.Location = New System.Drawing.Point(6, 9)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(111, 41)
        Me.GroupControl3.TabIndex = 24
        Me.GroupControl3.Text = "تاريخ الاستلام"
        '
        'GroupControl4
        '
        Me.GroupControl4.Controls.Add(Me.ProductionDateDateTimePicker)
        Me.GroupControl4.Location = New System.Drawing.Point(115, 9)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(122, 41)
        Me.GroupControl4.TabIndex = 25
        Me.GroupControl4.Text = "تاريخ التصنيع"
        '
        'GroupControl5
        '
        Me.GroupControl5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl5.Controls.Add(Me.OrderIDTextBox)
        Me.GroupControl5.Location = New System.Drawing.Point(690, 9)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(77, 41)
        Me.GroupControl5.TabIndex = 26
        Me.GroupControl5.Text = "كود الطلبيه"
        '
        'GroupControl6
        '
        Me.GroupControl6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl6.Controls.Add(Me.GroupCompoBox)
        Me.GroupControl6.Location = New System.Drawing.Point(568, 9)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(117, 41)
        Me.GroupControl6.TabIndex = 27
        Me.GroupControl6.Text = "فئه الطلبيه"
        '
        'GroupControl7
        '
        Me.GroupControl7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl7.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.GroupControl7.Controls.Add(Me.SimpleButton1)
        Me.GroupControl7.Controls.Add(Me.OrderProductComboBox)
        Me.GroupControl7.Controls.Add(Me.OrderQuntityTextBox)
        Me.GroupControl7.Controls.Add(Me.Button1)
        Me.GroupControl7.Location = New System.Drawing.Point(6, 57)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(935, 47)
        Me.GroupControl7.TabIndex = 102
        Me.GroupControl7.Text = "الصنف                                                                الكميه"
        '
        'GroupControl8
        '
        Me.GroupControl8.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupControl8.Controls.Add(Me.GaugeControl1)
        Me.GroupControl8.Controls.Add(Me.Label6)
        Me.GroupControl8.Controls.Add(Me.Label9)
        Me.GroupControl8.Controls.Add(Me.Label4)
        Me.GroupControl8.Controls.Add(Me.Label2)
        Me.GroupControl8.Controls.Add(Me.Label5)
        Me.GroupControl8.Controls.Add(Me.DelivaryDriverComboBox)
        Me.GroupControl8.Controls.Add(Me.Label8)
        Me.GroupControl8.Controls.Add(DelivaryDriverLabel)
        Me.GroupControl8.Controls.Add(Me.Label3)
        Me.GroupControl8.Controls.Add(Me.DelivaryCarComboBox)
        Me.GroupControl8.Controls.Add(Me.Label1)
        Me.GroupControl8.Controls.Add(DelivaryCarLabel)
        Me.GroupControl8.Controls.Add(Me.TextBox4)
        Me.GroupControl8.Controls.Add(Me.TextBox5)
        Me.GroupControl8.Controls.Add(Me.TextBox6)
        Me.GroupControl8.Controls.Add(Me.TextBox3)
        Me.GroupControl8.Location = New System.Drawing.Point(6, 278)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(512, 119)
        Me.GroupControl8.TabIndex = 103
        Me.GroupControl8.Text = "بيانات التحميل"
        '
        'NewOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(945, 408)
        Me.Controls.Add(Me.GroupControl8)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.GroupControl6)
        Me.Controls.Add(Me.ProductLineTextBox)
        Me.Controls.Add(Me.GroupControl7)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.Button4)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "NewOrder"
        Me.Opacity = 0R
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Text = "طلبيه جديده "
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.circularGauge1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateImageIndicatorComponent1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ArcScaleComponent1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ArcScaleRangeBarComponent1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        Me.GroupControl7.PerformLayout()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        Me.GroupControl8.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents OrderIDTextBox As TextBox
    Friend WithEvents OrderClintComboBox As ComboBox
    Friend WithEvents OrderProductComboBox As ComboBox
    Friend WithEvents OrderQuntityTextBox As TextBox
    Friend WithEvents ProductLineTextBox As TextBox
    Friend WithEvents DelivaryCarComboBox As ComboBox
    Friend WithEvents DelivaryDriverComboBox As ComboBox
    Friend WithEvents OrderDateDateTimePicker As DateTimePicker
    Friend WithEvents DeliveryDateDateTimePicker As DateTimePicker
    Friend WithEvents ProductionDateDateTimePicker As DateTimePicker
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents GaugeControl1 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents circularGauge1 As DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge
    Private WithEvents ArcScaleRangeBarComponent1 As DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleRangeBarComponent
    Private WithEvents ArcScaleComponent1 As DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent
    Private WithEvents StateImageIndicatorComponent1 As DevExpress.XtraGauges.Win.Base.StateImageIndicatorComponent
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents GroupCompoBox As ComboBox
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
End Class
