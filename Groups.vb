﻿Imports System.Data.SqlClient

Public Class Groups
    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        ' Save

        GroupDT.Rows.Add()
        Dim last As Integer = GroupDT.Rows.Count - 1
        GroupDT.Rows(last).Item("GroupName") = TextBox1.Text
        Dim save As New SqlCommandBuilder(GroupDA)
        GroupDA.Update(GroupDT)
        GroupDT.AcceptChanges()
        Log_UserAct(Me.Text, "اضافه", "", TextBox1.Text)



    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        ' Delete
        Dim pos As Integer = BindingContext(GroupDT).Position
        GroupDT.Rows(pos).Delete()
        Dim save As New SqlCommandBuilder(GroupDA)
        GroupDA.Update(GroupDT)
        GroupDT.AcceptChanges()
        Log_UserAct(Me.Text, "حذف", "", TextBox1.Text)



    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        ' New
        TextBox1.Text = ""

    End Sub

    Private Sub Groups_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LOAD_Groups()
        DataGridView1.DataSource = GroupDT
        DataGridView1.Columns(0).HeaderText = "الفئة"



    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged

        Try
            Dim pos As Integer = BindingContext(GroupDT).Position
            TextBox1.Text = GroupDT.Rows(pos).Item("GroupName")

        Catch


        End Try

    End Sub
End Class