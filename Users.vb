﻿Imports System.Data.SqlClient

Public Class Users


    Public usersDA As New SqlDataAdapter
    Public UsersDT As New DataTable
    Private Sub Users_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        usersDA = New SqlDataAdapter("Select * from Users", SQlManeCon)

        UsersDT.Clear()
        usersDA.Fill(UsersDT)
        DataGridView1.DataSource = UsersDT

        DataGridView1.Columns("UserPassWord").Visible = False
        DataGridView1.Columns("SettingMW").Visible = False
        DataGridView1.Columns("GroupW").Visible = False
        DataGridView1.Columns("DriverW").Visible = False
        DataGridView1.Columns("CarsW").Visible = False
        DataGridView1.Columns("ProductW").Visible = False
        DataGridView1.Columns("ProductionLineW").Visible = False
        DataGridView1.Columns("OrdersMW").Visible = False
        DataGridView1.Columns("CarTableW").Visible = False
        DataGridView1.Columns("NewOrderW").Visible = False
        DataGridView1.Columns("OrdersW").Visible = False
        DataGridView1.Columns("OrdersWEdit").Visible = False
        DataGridView1.Columns("OrdersWDelete").Visible = False
        DataGridView1.Columns("OrdersWPrint").Visible = False
        DataGridView1.Columns("DoneOrdersW").Visible = False
        DataGridView1.Columns("ProductionMW").Visible = False
        DataGridView1.Columns("ProductionTableW").Visible = False
        DataGridView1.Columns("UserMW").Visible = False
        DataGridView1.Columns("UserW").Visible = False
        DataGridView1.Columns("UserHSTW").Visible = False

        DataGridView1.Columns("UserName").HeaderText = "المستخدم"


    End Sub

    Private Sub UserNameTextEdit_EditValueChanged(sender As Object, e As EventArgs) Handles UserNameTextEdit.TextChanged

        If UserNameTextEdit.Text = "Admin" Then
            UserNameTextEdit.Enabled = False
            GroupBox2.Enabled = False
            SimpleButton2.Enabled = False

        Else
            UserNameTextEdit.Enabled = True
            GroupBox2.Enabled = True
            SimpleButton2.Enabled = True

        End If
    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        usersDA = New SqlDataAdapter("Select * from Users", SQlManeCon)

        UsersDT.Clear()
        usersDA.Fill(UsersDT)
        DataGridView1.DataSource = UsersDT
        UserNameTextEdit.Text = ""
        UserPassWordTextEdit.Text = ""

    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click



        Try


            If UserPassWordTextEdit.Text <> Nothing And UserPassWordTextEdit.Text <> Nothing Then

                Dim CHKUSERDA As New SqlDataAdapter
                Dim CHKUSERDT As New DataTable
                CHKUSERDA = New SqlDataAdapter("Select * from Users Where UserName = N'" & UserNameTextEdit.Text & "' ", SQlManeCon)
                CHKUSERDT.Clear()
                CHKUSERDA.Fill(CHKUSERDT)
                If CHKUSERDT.Rows.Count = 0 Then
                    UsersDT.Rows.Add()
                    Dim last As Integer = UsersDT.Rows.Count - 1





                    UsersDT.Rows(last).Item("UserName") = UserNameTextEdit.Text
                    UsersDT.Rows(last).Item("UserPassWord") = UserPassWordTextEdit.Text
                    UsersDT.Rows(last).Item("SettingMW") = SettingMWCheckEdit.Checked
                    UsersDT.Rows(last).Item("GroupW") = GroupWCheckEdit.Checked
                    UsersDT.Rows(last).Item("DriverW") = DriverWCheckEdit.Checked
                    UsersDT.Rows(last).Item("CarsW") = CarsWCheckEdit.Checked
                    UsersDT.Rows(last).Item("ProductW") = ProductWCheckEdit.Checked
                    UsersDT.Rows(last).Item("ProductionLineW") = ProductionLineWCheckEdit.Checked
                    UsersDT.Rows(last).Item("OrdersMW") = OrdersMWCheckEdit.Checked
                    UsersDT.Rows(last).Item("CarTableW") = CarTableWCheckEdit.Checked
                    UsersDT.Rows(last).Item("NewOrderW") = NewOrderWCheckEdit.Checked
                    UsersDT.Rows(last).Item("OrdersW") = OrdersWCheckEdit.Checked
                    UsersDT.Rows(last).Item("OrdersWEdit") = OrdersWEditCheckEdit.Checked
                    UsersDT.Rows(last).Item("OrdersWDelete") = OrdersWDeleteCheckEdit.Checked
                    UsersDT.Rows(last).Item("OrdersWPrint") = OrdersWPrintCheckEdit.Checked
                    UsersDT.Rows(last).Item("DoneOrdersW") = DoneOrdersWCheckEdit.Checked
                    UsersDT.Rows(last).Item("ProductionMW") = ProductionMWCheckEdit.Checked
                    UsersDT.Rows(last).Item("ProductionTableW") = ProductionTableWCheckEdit.Checked
                    UsersDT.Rows(last).Item("UserMW") = UserMWCheckEdit.Checked
                    UsersDT.Rows(last).Item("UserW") = UserWCheckEdit.Checked
                    UsersDT.Rows(last).Item("UserHSTW") = UserHSTWCheckEdit.Checked


                    Dim save As New SqlCommandBuilder(usersDA)
                    usersDA.Update(UsersDT)
                    UsersDT.AcceptChanges()

                    usersDA = New SqlDataAdapter("Select * from Users", SQlManeCon)

                    UsersDT.Clear()
                    usersDA.Fill(UsersDT)
                    DataGridView1.DataSource = UsersDT
                ElseIf CHKUSERDT.Rows.Count > 0 Then
                    Dim pos As Integer = BindingContext(UsersDT).Position

                    UsersDT.Rows(pos).Item("UserName") = UserNameTextEdit.Text
                    UsersDT.Rows(pos).Item("UserPassWord") = UserPassWordTextEdit.Text
                    UsersDT.Rows(pos).Item("SettingMW") = SettingMWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("GroupW") = GroupWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("DriverW") = DriverWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("CarsW") = CarsWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("ProductW") = ProductWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("ProductionLineW") = ProductionLineWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("OrdersMW") = OrdersMWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("CarTableW") = CarTableWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("NewOrderW") = NewOrderWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("OrdersW") = OrdersWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("OrdersWEdit") = OrdersWEditCheckEdit.Checked
                    UsersDT.Rows(pos).Item("OrdersWDelete") = OrdersWDeleteCheckEdit.Checked
                    UsersDT.Rows(pos).Item("OrdersWPrint") = OrdersWPrintCheckEdit.Checked
                    UsersDT.Rows(pos).Item("DoneOrdersW") = DoneOrdersWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("ProductionMW") = ProductionMWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("ProductionTableW") = ProductionTableWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("UserMW") = UserMWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("UserW") = UserWCheckEdit.Checked
                    UsersDT.Rows(pos).Item("UserHSTW") = UserHSTWCheckEdit.Checked


                    Dim save1 As New SqlCommandBuilder(usersDA)
                    usersDA.Update(UsersDT)
                    UsersDT.AcceptChanges()

                    usersDA = New SqlDataAdapter("Select * from Users", SQlManeCon)

                    UsersDT.Clear()
                    usersDA.Fill(UsersDT)
                    DataGridView1.DataSource = UsersDT

                End If
            End If
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        Try
            Dim pos As Integer = BindingContext(UsersDT).Position
            UsersDT.Rows(pos).Delete()
            Dim save As New SqlCommandBuilder(usersDA)
            usersDA.Update(UsersDT)
            UsersDT.AcceptChanges()
            usersDA = New SqlDataAdapter("Select * from Users", SQlManeCon)

            UsersDT.Clear()
            usersDA.Fill(UsersDT)
            DataGridView1.DataSource = UsersDT
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged

        Try
            Dim pos As Integer = BindingContext(UsersDT).Position
            UserNameTextEdit.Text = UsersDT.Rows(pos).Item("UserName")
            UserPassWordTextEdit.Text = UsersDT.Rows(pos).Item("UserPassWord")
            SettingMWCheckEdit.Checked = UsersDT.Rows(pos).Item("SettingMW")
            GroupWCheckEdit.Checked = UsersDT.Rows(pos).Item("GroupW")
            DriverWCheckEdit.Checked = UsersDT.Rows(pos).Item("DriverW")
            CarsWCheckEdit.Checked = UsersDT.Rows(pos).Item("CarsW")
            ProductWCheckEdit.Checked = UsersDT.Rows(pos).Item("ProductW")
            ProductionLineWCheckEdit.Checked = UsersDT.Rows(pos).Item("ProductionLineW")
            OrdersMWCheckEdit.Checked = UsersDT.Rows(pos).Item("OrdersMW")
            CarTableWCheckEdit.Checked = UsersDT.Rows(pos).Item("CarTableW")
            NewOrderWCheckEdit.Checked = UsersDT.Rows(pos).Item("NewOrderW")
            OrdersWCheckEdit.Checked = UsersDT.Rows(pos).Item("OrdersW")
            OrdersWEditCheckEdit.Checked = UsersDT.Rows(pos).Item("OrdersWEdit")
            OrdersWDeleteCheckEdit.Checked = UsersDT.Rows(pos).Item("OrdersWDelete")
            OrdersWPrintCheckEdit.Checked = UsersDT.Rows(pos).Item("OrdersWPrint")
            DoneOrdersWCheckEdit.Checked = UsersDT.Rows(pos).Item("DoneOrdersW")
            ProductionMWCheckEdit.Checked = UsersDT.Rows(pos).Item("ProductionMW")
            ProductionTableWCheckEdit.Checked = UsersDT.Rows(pos).Item("ProductionTableW")
            UserMWCheckEdit.Checked = UsersDT.Rows(pos).Item("UserMW")
            UserWCheckEdit.Checked = UsersDT.Rows(pos).Item("UserW")
            UserHSTWCheckEdit.Checked = UsersDT.Rows(pos).Item("UserHSTW")
        Catch
        End Try


    End Sub

End Class