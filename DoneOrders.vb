﻿
Imports System.Data.SqlClient
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraPrintingLinks
Imports DevExpress.Utils

Public Class DoneOrders

    Private Sub Orders_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Visible = False
        Try
            DateTimePicker1.Value = Today
            DateTimePicker2.Value = Today

            If CheckButton1.Checked = True Then
                DateTimePicker1.Enabled = True
                DoneOrderDA = New SqlDataAdapter("Select * from Orders where DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "' and IsDeliverd ='True' ", SQlManeCon)
                DoneOrderDT.Clear()
                DoneOrderDA.Fill(DoneOrderDT)


            End If
            If CheckButton1.Checked = False Then
                DateTimePicker1.Value = Today
                DateTimePicker1.Enabled = False
                Load_DoneOrders()

            End If
            'DateTimePicker1_ValueChanged(sender, e)

            GridControl1.BeginUpdate()
            Try
                GridView1.Columns.Clear()
                GridControl1.DataSource = Nothing
                GridControl1.DataSource = DoneOrderDT

            Finally
                GridControl1.EndUpdate()

            End Try

            GridView1.Columns("IsDeliverd").Visible = False
            GridView1.Columns("ProductLine").Visible = False
            GridView1.Columns("ProductionDate").Visible = False
            GridView1.Columns("OrderInton").Visible = False
            GridView1.Columns("CusCode").Visible = False
            GridView1.Columns("Id").Visible = False
            GridView1.Columns("InvoiceCode").Visible = True


            GridView1.Columns("DeliveryDate").Caption = "موعد الاستلام"
            GridView1.Columns("OrderID").Caption = "م"
            GridView1.Columns("OrderID").Width = 20
            GridView1.Columns("OrderClint").Caption = "العميل"
            GridView1.Columns("OrderClint").Width = 120
            GridView1.Columns("OrderProduct").Caption = "الصنف"
            GridView1.Columns("OrderQuntity").Caption = "الكميه"
            GridView1.Columns("OrderQuntity").Width = 40
            GridView1.Columns("ProductLine").Caption = "خط الانتاج"
            GridView1.Columns("DeliveryCar").Caption = "السياره"
            GridView1.Columns("DeliveryDriver").Caption = "السائق"
            GridView1.Columns("OrderDate").Caption = "تاريخ الطلب"
            GridView1.Columns("InvoiceCode").Caption = "رقم فاتوره التسليم"
            GridView1.Columns("OrderDate").DisplayFormat.FormatString = "yyyy-MM-dd hh:mm"


            GridView1.ClearSorting()
            GridView1.Columns("OrderID").SortOrder = DevExpress.Data.ColumnSortOrder.Descending



            GridView1.Columns(0).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True
            GridView1.Columns(1).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True

            GridView1.Columns(2).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView1.Columns(3).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView1.Columns(4).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False

            GridView1.Columns(7).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView1.Columns(8).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
            GridView1.Columns(9).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False

            GridView1.Columns(10).OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            ' MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click

        Try
            Dim composLink As CompositeLink = New CompositeLink(New PrintingSystem())
            AddHandler composLink.CreateMarginalHeaderArea, AddressOf composLink_CreateMarginalHeaderArea
            Dim pcLink1 As PrintableComponentLink = New PrintableComponentLink()
            Dim pcLink2 As PrintableComponentLink = New PrintableComponentLink()
            Dim linkMainReport As Link = New Link()
            AddHandler linkMainReport.CreateDetailArea, AddressOf linkMainReport_CreateDetailArea
            Dim linkGrid1Report As Link = New Link()
            AddHandler linkGrid1Report.CreateDetailArea, AddressOf linkGrid1Report_CreateDetailArea
            Dim linkGrid2Report As Link = New Link()
            AddHandler linkGrid2Report.CreateDetailArea, AddressOf linkGrid2Report_CreateDetailArea

            ' Assign the controls to the printing links.
            pcLink1.Component = Me.GridControl1


            ' Populate the collection of links in the composite link.
            ' The order of operations corresponds to the document structure.
            composLink.Links.Add(linkGrid1Report)
            composLink.Links.Add(pcLink1)
            composLink.Links.Add(linkMainReport)
            composLink.Links.Add(linkGrid2Report)

            'composLink.PaperKind = Printing.PaperKind.A5
            composLink.PaperKind = Printing.PaperKind.A4
            composLink.Margins.Left = 38
            composLink.Margins.Right = 38
            composLink.Margins.Bottom = 30
            composLink.Margins.Top = 30


            ' Create the report and show the preview window.
            composLink.ShowPreviewDialog()



        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try


    End Sub

    ' Inserts a PageInfoBrick into the top margin to display the time.
    Private Sub composLink_CreateMarginalHeaderArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'e.Graph.DrawPageInfo(PageInfo.DateTime, "{0:hhhh:mmmm:ssss}",
        'Color.Black, New RectangleF(0, 0, 200, 50), BorderSide.None)
    End Sub

    ' Creates a text header for the first grid.
    Private Sub linkGrid1Report_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)

        Try


            Dim tb As TextBrick = New TextBrick()


            If CheckButton1.Checked = True Then
                tb.Text = "جدول الطلبيات التي تم تسليمها من تاريخ   " & DateTimePicker1.Value.Date & "  حتي تاريخ  " & DateTimePicker2.Value.Date & ""
            Else
                tb.Text = "جدول جميع الطلبيات التي تم تسليمها "
            End If

            tb.Font = New Font("Times New Roman", 15, FontStyle.Bold)
            tb.Rect = New RectangleF(0, 0, 720, 25)
            tb.BorderWidth = 2
            tb.BackColor = Color.LightGray
            tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Center

            tb.VertAlignment = DevExpress.Utils.VertAlignment.Center

            e.Graph.DrawBrick(tb)
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            '  MsgBox(ex.Message & "log is saved in " & Application.StartupPath & "Log.txt")
        End Try
    End Sub

    '    ' Creates an interval between the grids and fills it with color.
    Private Sub linkMainReport_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'Dim tb As TextBrick = New TextBrick()
        'tb.Rect = New RectangleF(0, 0, e.Graph.ClientPageSize.Width, 50)
        'tb.BackColor = Color.Gray
        'e.Graph.DrawBrick(tb)
    End Sub

    ' Creates a text header for the second grid.
    Private Sub linkGrid2Report_CreateDetailArea(ByVal sender As Object, ByVal e As CreateAreaEventArgs)
        'Dim tb As TextBrick = New TextBrick()
        'tb.Text = "Suppliers"
        'tb.Font = New Font("Arial", 15)
        'tb.Rect = New RectangleF(0, 0, 300, 25)
        'tb.BorderWidth = 0
        'tb.BackColor = Color.Transparent
        'tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Near
        'e.Graph.DrawBrick(tb)
    End Sub


    Private Sub CheckButton1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckButton1.CheckedChanged

        Try


            If CheckButton1.Checked = True Then
                DateTimePicker1.Enabled = True
                DateTimePicker2.Enabled = True
                DoneOrderDA = New SqlDataAdapter("Select * from Orders where DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & DateTimePicker2.Value.Month & DateTimePicker2.Value.Day & "' and IsDeliverd ='True'  ", SQlManeCon)

                DoneOrderDT.Clear()
                DoneOrderDA.Fill(DoneOrderDT)

            End If
            If CheckButton1.Checked = False Then
                DateTimePicker1.Enabled = False
                Load_DoneOrders()

            End If
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker1.ValueChanged, DateTimePicker2.ValueChanged


        Try



            DoneOrderDA = New SqlDataAdapter("Select * from Orders where DeliveryDate between '" & DateTimePicker1.Value.Year & "-" & DateTimePicker1.Value.Month & "-" & DateTimePicker1.Value.Day & "' and  '" & DateTimePicker2.Value.Year & "-" & DateTimePicker2.Value.Month & "-" & DateTimePicker2.Value.Day & "'  and IsDeliverd ='true' ", SQlManeCon)

            DoneOrderDT.Clear()
            DoneOrderDA.Fill(DoneOrderDT)

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try



    End Sub




    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click

        Try


            If MsgBox("هل انت متآكد من حذف هذه الطلبيه", MsgBoxStyle.YesNo, "تأكيد الحذف") = MsgBoxResult.Yes Then
                Dim str As String
                str = GridView1.GetFocusedRowCellDisplayText("OrderID")
                Dim tmporderda As New SqlDataAdapter("Select * from ordershead Where OrderID like '" & str & "' ", SQlManeCon)
                Dim tmporderdt As New DataTable
                tmporderdt.Clear()
                tmporderda.Fill(tmporderdt)
                For i As Integer = tmporderdt.Rows.Count - 1 To 0 Step -1
                    tmporderdt.Rows(i).Delete()
                Next
                Dim save As New SqlCommandBuilder(tmporderda)
                tmporderda.Update(tmporderdt)
                tmporderdt.AcceptChanges()
                CheckButton1_CheckedChanged(sender, e)
                Log_UserAct(Me.Text, "حذف", GridView1.GetFocusedRowCellDisplayText("OrderID"), GridView1.GetFocusedRowCellDisplayText("OrderClint"))

            End If
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub GridControl1_DoubleClick(sender As Object, e As EventArgs) Handles GridControl1.DoubleClick
        'EditOrder.Close()


        EditOrder.OrderIDTextBox.Text = GridView1.GetFocusedRowCellValue("OrderID")
        EditOrder.OrderClintComboBox.Text = GridView1.GetFocusedRowCellValue("OrderClint")
        EditOrder.OrderDateDateTimePicker.Value = GridView1.GetFocusedRowCellValue("OrderDate")
        EditOrder.DelivaryCarComboBox.Text = GridView1.GetFocusedRowCellDisplayText("DeliveryCar")
        EditOrder.DelivaryDriverComboBox.Text = GridView1.GetFocusedRowCellDisplayText("DeliveryDriver")
        EditOrder.ProductionDateDateTimePicker.Value = GridView1.GetFocusedRowCellValue("ProductionDate")
        EditOrder.DeliveryDateDateTimePicker.Value = GridView1.GetFocusedRowCellValue("DeliveryDate")

        EditOrder.TextBox2.Text = GridView1.GetFocusedRowCellValue("CusCode")

        EditOrder.Show()

        '====== Next Is Handeld in EditeOrder Form 




    End Sub


    Public Sub int_load()
        InitializeComponent()
    End Sub


End Class