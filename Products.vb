﻿Imports System.Data.SqlClient

Public Class Products
    Private Sub Products_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try


            Me.Visible = False
            Dim da As New SqlDataAdapter("SELECT [ItemId],[ItemNameAr],[IsDeleted] ,[ItemType] FROM [ERP].[dbo].[IC_Item]where ItemType = 2 and IsDeleted = 0", SQLCon)
            Dim dt As New DataTable
            da.Fill(dt)
            ComboBox1.DataSource = dt
            ComboBox1.DisplayMember = "ItemNameAr"
            ComboBox1.ValueMember = "ItemId"

            Load_Product()
            DataGridView1.DataSource = Nothing
            DataGridView1.DataSource = ProductDT
            DataGridView1.Columns("ProductName").HeaderText = "الصنف"
            DataGridView1.Columns("ProductionLine").HeaderText = "خط الانتاج"
            DataGridView1.Columns("ProductUnit").HeaderText = "الوحده"
            DataGridView1.Columns("ProductUnitsInTon").HeaderText = "وحده ف الطن"
            DataGridView1.Columns("Sort").HeaderText = "الترتيب"
            DataGridView1.Columns("ProductID").HeaderText = "الكود"
            Load_PLine()
            ProductionLineTextBox.DataSource = PLineDT
            ProductionLineTextBox.DisplayMember = "LineName"


        Catch ex As Exception

        End Try

    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click

        Try

            If ProductNameTextBox.Text <> Nothing Then

                Dim CHKPRDDA As New SqlDataAdapter
                Dim CHKPRDDT As New DataTable
                CHKPRDDA = New SqlDataAdapter("select * from product Where productname Like '" & ProductNameTextBox.Text & "' ", SQlManeCon)
                CHKPRDDT.Clear()
            CHKPRDDA.Fill(CHKPRDDT)
            If CHKPRDDT.Rows.Count = 0 Then
                ProductDT.Rows.Add()
                Dim last As Integer = ProductDT.Rows.Count - 1
                ProductDT.Rows(last).Item("ProductName") = ProductNameTextBox.Text
                ProductDT.Rows(last).Item("ProductionLine") = ProductionLineTextBox.Text
                ProductDT.Rows(last).Item("ProductUnit") = ProductUnitTextBox.Text
                    ProductDT.Rows(last).Item("ProductUnitsInTon") = ProductUnitsInTonTextBox.Text
                    ProductDT.Rows(last).Item("sort") = NumericUpDown1.Value
                    ProductDT.Rows(last).Item("ProductID") = ComboBox1.SelectedValue
                    Dim save As New SqlCommandBuilder(ProductDA)
                    ProductDA.Update(ProductDT)
                ProductDT.AcceptChanges()
                Load_Product()

            ElseIf CHKPRDDT.Rows.Count > 0 Then
                Dim pos As Integer = BindingContext(ProductDT).Position
                ProductDT.Rows(pos).Item("ProductName") = ProductNameTextBox.Text
                ProductDT.Rows(pos).Item("ProductionLine") = ProductionLineTextBox.Text
                ProductDT.Rows(pos).Item("ProductUnit") = ProductUnitTextBox.Text
                    ProductDT.Rows(pos).Item("ProductUnitsInTon") = ProductUnitsInTonTextBox.Text
                    ProductDT.Rows(pos).Item("sort") = NumericUpDown1.Value
                    ProductDT.Rows(pos).Item("ProductID") = ComboBox1.SelectedValue

                    Dim save As New SqlCommandBuilder(ProductDA)
                    ProductDA.Update(ProductDT)
                ProductDT.AcceptChanges()
                Load_Product()
            End If
        End If

        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try

    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click

        Try
            Dim pos As Integer = BindingContext(ProductDT).Position
            ProductDT.Rows(pos).Delete()
            Dim save As New SqlCommandBuilder(ProductDA)
            ProductDA.Update(ProductDT)
            ProductDT.AcceptChanges()
            Load_Product()
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try


    End Sub

    Private Sub ToolStripButton3_Click(sender As Object, e As EventArgs) Handles ToolStripButton3.Click

        ProductNameTextBox.Text = ""
        ProductionLineTextBox.Text = ""
        ProductUnitTextBox.Text = ""
        ProductUnitsInTonTextBox.Text = ""
        ComboBox1.Text = ""

    End Sub

    'Private Sub DataGridView1_SelectionChanged(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.SelectionChanged
    ' 
    'End Sub

    Private Sub ProductUnitsInTonTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ProductUnitsInTonTextBox.KeyPress
        Try
            If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
                e.Handled = True
                'MsgBox("هذا الحقل يقبل ارقام فقط")
            Else

            End If


        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try

    End Sub

    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Dim pos As Integer = BindingContext(ProductDT).Position

            ProductNameTextBox.Text = ProductDT.Rows(pos).Item("ProductName")
            ProductionLineTextBox.Text = ProductDT.Rows(pos).Item("ProductionLine")
            ProductUnitTextBox.Text = ProductDT.Rows(pos).Item("ProductUnit")
            ProductUnitsInTonTextBox.Text = ProductDT.Rows(pos).Item("ProductUnitsInTon")
            NumericUpDown1.Value = ProductDT.Rows(pos).Item("sort")
            ComboBox1.SelectedValue = ProductDT.Rows(pos).Item("ProductID")
        Catch ex As Exception

        End Try
    End Sub
End Class