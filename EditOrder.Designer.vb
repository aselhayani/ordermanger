﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class EditOrder
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim OrderDateLabel As System.Windows.Forms.Label
        Dim OrderClintLabel As System.Windows.Forms.Label
        Dim DeliveryDateLabel As System.Windows.Forms.Label
        Dim ProductionDateLabel As System.Windows.Forms.Label
        Dim OrderQuntityLabel As System.Windows.Forms.Label
        Dim OrderProductLabel As System.Windows.Forms.Label
        Dim DelivaryCarLabel As System.Windows.Forms.Label
        Dim DelivaryDriverLabel As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim ImageIndicatorState2 As DevExpress.XtraGauges.Core.Model.ImageIndicatorState = New DevExpress.XtraGauges.Core.Model.ImageIndicatorState()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EditOrder))
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.OrderIDTextBox = New System.Windows.Forms.TextBox()
        Me.ProductLineTextBox = New System.Windows.Forms.TextBox()
        Me.OrderDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.OrderClintComboBox = New System.Windows.Forms.ComboBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DeliveryDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ProductionDateDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.NewButton = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.OrderQuntityTextBox = New System.Windows.Forms.TextBox()
        Me.OrderProductComboBox = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GaugeControl1 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.circularGauge1 = New DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge()
        Me.StateImageIndicatorComponent1 = New DevExpress.XtraGauges.Win.Base.StateImageIndicatorComponent()
        Me.ArcScaleComponent1 = New DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ArcScaleRangeBarComponent1 = New DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleRangeBarComponent()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DelivaryCarComboBox = New System.Windows.Forms.ComboBox()
        Me.DelivaryDriverComboBox = New System.Windows.Forms.ComboBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.GroupCompoBox = New System.Windows.Forms.ComboBox()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        OrderDateLabel = New System.Windows.Forms.Label()
        OrderClintLabel = New System.Windows.Forms.Label()
        DeliveryDateLabel = New System.Windows.Forms.Label()
        ProductionDateLabel = New System.Windows.Forms.Label()
        OrderQuntityLabel = New System.Windows.Forms.Label()
        OrderProductLabel = New System.Windows.Forms.Label()
        DelivaryCarLabel = New System.Windows.Forms.Label()
        DelivaryDriverLabel = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.circularGauge1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateImageIndicatorComponent1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ArcScaleComponent1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ArcScaleRangeBarComponent1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OrderDateLabel
        '
        OrderDateLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        OrderDateLabel.AutoSize = True
        OrderDateLabel.Location = New System.Drawing.Point(292, 10)
        OrderDateLabel.Name = "OrderDateLabel"
        OrderDateLabel.Size = New System.Drawing.Size(59, 13)
        OrderDateLabel.TabIndex = 26
        OrderDateLabel.Text = "تاريخ الطلب"
        '
        'OrderClintLabel
        '
        OrderClintLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        OrderClintLabel.AutoSize = True
        OrderClintLabel.Location = New System.Drawing.Point(541, 34)
        OrderClintLabel.Name = "OrderClintLabel"
        OrderClintLabel.Size = New System.Drawing.Size(63, 13)
        OrderClintLabel.TabIndex = 4
        OrderClintLabel.Text = "اسم العميل"
        '
        'DeliveryDateLabel
        '
        DeliveryDateLabel.AutoSize = True
        DeliveryDateLabel.Location = New System.Drawing.Point(145, 75)
        DeliveryDateLabel.Name = "DeliveryDateLabel"
        DeliveryDateLabel.Size = New System.Drawing.Size(74, 13)
        DeliveryDateLabel.TabIndex = 18
        DeliveryDateLabel.Text = "موعد الاستلام"
        '
        'ProductionDateLabel
        '
        ProductionDateLabel.AutoSize = True
        ProductionDateLabel.Location = New System.Drawing.Point(145, 44)
        ProductionDateLabel.Name = "ProductionDateLabel"
        ProductionDateLabel.Size = New System.Drawing.Size(67, 13)
        ProductionDateLabel.TabIndex = 20
        ProductionDateLabel.Text = "موعد التصنيع"
        '
        'OrderQuntityLabel
        '
        OrderQuntityLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        OrderQuntityLabel.AutoSize = True
        OrderQuntityLabel.Location = New System.Drawing.Point(330, 67)
        OrderQuntityLabel.Name = "OrderQuntityLabel"
        OrderQuntityLabel.Size = New System.Drawing.Size(36, 13)
        OrderQuntityLabel.TabIndex = 8
        OrderQuntityLabel.Text = "الكميه"
        '
        'OrderProductLabel
        '
        OrderProductLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        OrderProductLabel.AutoSize = True
        OrderProductLabel.Location = New System.Drawing.Point(561, 68)
        OrderProductLabel.Name = "OrderProductLabel"
        OrderProductLabel.Size = New System.Drawing.Size(36, 13)
        OrderProductLabel.TabIndex = 6
        OrderProductLabel.Text = "الصنف"
        '
        'DelivaryCarLabel
        '
        DelivaryCarLabel.AutoSize = True
        DelivaryCarLabel.Location = New System.Drawing.Point(312, 21)
        DelivaryCarLabel.Name = "DelivaryCarLabel"
        DelivaryCarLabel.Size = New System.Drawing.Size(41, 13)
        DelivaryCarLabel.TabIndex = 12
        DelivaryCarLabel.Text = "السياره"
        '
        'DelivaryDriverLabel
        '
        DelivaryDriverLabel.AutoSize = True
        DelivaryDriverLabel.Location = New System.Drawing.Point(313, 47)
        DelivaryDriverLabel.Name = "DelivaryDriverLabel"
        DelivaryDriverLabel.Size = New System.Drawing.Size(41, 13)
        DelivaryDriverLabel.TabIndex = 14
        DelivaryDriverLabel.Text = "السائق"
        '
        'Label7
        '
        Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Label7.AutoSize = True
        Label7.Location = New System.Drawing.Point(542, 378)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(57, 13)
        Label7.TabIndex = 42
        Label7.Text = "فئه الطلبيه"
        '
        'TextBox1
        '
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(7, 31)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(17, 20)
        Me.TextBox1.TabIndex = 28
        Me.TextBox1.Visible = False
        '
        'OrderIDTextBox
        '
        Me.OrderIDTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OrderIDTextBox.Enabled = False
        Me.OrderIDTextBox.Location = New System.Drawing.Point(12, 5)
        Me.OrderIDTextBox.Name = "OrderIDTextBox"
        Me.OrderIDTextBox.Size = New System.Drawing.Size(129, 20)
        Me.OrderIDTextBox.TabIndex = 24
        '
        'ProductLineTextBox
        '
        Me.ProductLineTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProductLineTextBox.Enabled = False
        Me.ProductLineTextBox.Location = New System.Drawing.Point(44, 31)
        Me.ProductLineTextBox.Name = "ProductLineTextBox"
        Me.ProductLineTextBox.Size = New System.Drawing.Size(22, 20)
        Me.ProductLineTextBox.TabIndex = 25
        Me.ProductLineTextBox.Visible = False
        '
        'OrderDateDateTimePicker
        '
        Me.OrderDateDateTimePicker.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OrderDateDateTimePicker.CustomFormat = "yyyy/MM/dd  hh:mm"
        Me.OrderDateDateTimePicker.Enabled = False
        Me.OrderDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.OrderDateDateTimePicker.Location = New System.Drawing.Point(156, 7)
        Me.OrderDateDateTimePicker.Name = "OrderDateDateTimePicker"
        Me.OrderDateDateTimePicker.Size = New System.Drawing.Size(130, 20)
        Me.OrderDateDateTimePicker.TabIndex = 27
        '
        'Button1
        '
        Me.Button1.Enabled = False
        Me.Button1.Location = New System.Drawing.Point(78, 64)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(46, 21)
        Me.Button1.TabIndex = 23
        Me.Button1.Text = "اضافه"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'OrderClintComboBox
        '
        Me.OrderClintComboBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OrderClintComboBox.Enabled = False
        Me.OrderClintComboBox.FormattingEnabled = True
        Me.OrderClintComboBox.Location = New System.Drawing.Point(370, 30)
        Me.OrderClintComboBox.Name = "OrderClintComboBox"
        Me.OrderClintComboBox.Size = New System.Drawing.Size(165, 21)
        Me.OrderClintComboBox.TabIndex = 5
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(15, 91)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(591, 133)
        Me.DataGridView1.TabIndex = 32
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.GroupBox2.Controls.Add(DeliveryDateLabel)
        Me.GroupBox2.Controls.Add(Me.DeliveryDateDateTimePicker)
        Me.GroupBox2.Controls.Add(ProductionDateLabel)
        Me.GroupBox2.Controls.Add(Me.ProductionDateDateTimePicker)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(384, 250)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(222, 92)
        Me.GroupBox2.TabIndex = 38
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "بيانات التجهيز"
        '
        'DeliveryDateDateTimePicker
        '
        Me.DeliveryDateDateTimePicker.CustomFormat = "yyyy/MM/dd"
        Me.DeliveryDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DeliveryDateDateTimePicker.Location = New System.Drawing.Point(14, 69)
        Me.DeliveryDateDateTimePicker.Name = "DeliveryDateDateTimePicker"
        Me.DeliveryDateDateTimePicker.Size = New System.Drawing.Size(123, 20)
        Me.DeliveryDateDateTimePicker.TabIndex = 19
        '
        'ProductionDateDateTimePicker
        '
        Me.ProductionDateDateTimePicker.CustomFormat = "yyyy/MM/dd"
        Me.ProductionDateDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.ProductionDateDateTimePicker.Location = New System.Drawing.Point(14, 39)
        Me.ProductionDateDateTimePicker.Name = "ProductionDateDateTimePicker"
        Me.ProductionDateDateTimePicker.Size = New System.Drawing.Size(122, 20)
        Me.ProductionDateDateTimePicker.TabIndex = 21
        Me.ProductionDateDateTimePicker.Value = New Date(2017, 10, 11, 0, 0, 0, 0)
        '
        'NewButton
        '
        Me.NewButton.Location = New System.Drawing.Point(129, 64)
        Me.NewButton.Name = "NewButton"
        Me.NewButton.Size = New System.Drawing.Size(52, 21)
        Me.NewButton.TabIndex = 39
        Me.NewButton.Text = "جديد"
        Me.NewButton.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Red
        Me.Button3.Location = New System.Drawing.Point(15, 64)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(51, 21)
        Me.Button3.TabIndex = 39
        Me.Button3.Text = "حذف"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(186, 64)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(54, 21)
        Me.Button4.TabIndex = 23
        Me.Button4.Text = "تعديل"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'OrderQuntityTextBox
        '
        Me.OrderQuntityTextBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OrderQuntityTextBox.Location = New System.Drawing.Point(252, 64)
        Me.OrderQuntityTextBox.Name = "OrderQuntityTextBox"
        Me.OrderQuntityTextBox.Size = New System.Drawing.Size(72, 20)
        Me.OrderQuntityTextBox.TabIndex = 9
        '
        'OrderProductComboBox
        '
        Me.OrderProductComboBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OrderProductComboBox.Enabled = False
        Me.OrderProductComboBox.FormattingEnabled = True
        Me.OrderProductComboBox.Location = New System.Drawing.Point(370, 64)
        Me.OrderProductComboBox.Name = "OrderProductComboBox"
        Me.OrderProductComboBox.Size = New System.Drawing.Size(185, 21)
        Me.OrderProductComboBox.TabIndex = 7
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(385, 397)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(221, 34)
        Me.Button2.TabIndex = 40
        Me.Button2.Text = "حفظ"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton1.Location = New System.Drawing.Point(14, 228)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(364, 23)
        Me.SimpleButton1.TabIndex = 26
        Me.SimpleButton1.Text = "تغير بيانات التحميل"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton2.Location = New System.Drawing.Point(384, 228)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(222, 24)
        Me.SimpleButton2.TabIndex = 26
        Me.SimpleButton2.Text = "تغيير بيانات التسليم"
        '
        'TextBox2
        '
        Me.TextBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox2.Enabled = False
        Me.TextBox2.Location = New System.Drawing.Point(78, 31)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(39, 20)
        Me.TextBox2.TabIndex = 25
        Me.TextBox2.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.GaugeControl1)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(DelivaryCarLabel)
        Me.GroupBox1.Controls.Add(Me.DelivaryCarComboBox)
        Me.GroupBox1.Controls.Add(DelivaryDriverLabel)
        Me.GroupBox1.Controls.Add(Me.DelivaryDriverComboBox)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox6)
        Me.GroupBox1.Controls.Add(Me.TextBox5)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 257)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(363, 185)
        Me.GroupBox1.TabIndex = 41
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "بيانات التحميل"
        '
        'GaugeControl1
        '
        Me.GaugeControl1.ColorScheme.TargetElements = CType((DevExpress.XtraGauges.Core.Base.TargetElement.RangeBar Or DevExpress.XtraGauges.Core.Base.TargetElement.Label), DevExpress.XtraGauges.Core.Base.TargetElement)
        Me.GaugeControl1.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.circularGauge1})
        Me.GaugeControl1.Location = New System.Drawing.Point(6, 16)
        Me.GaugeControl1.Name = "GaugeControl1"
        Me.GaugeControl1.Size = New System.Drawing.Size(171, 158)
        Me.GaugeControl1.TabIndex = 31
        '
        'circularGauge1
        '
        Me.circularGauge1.Bounds = New System.Drawing.Rectangle(6, 6, 159, 146)
        Me.circularGauge1.ImageIndicators.AddRange(New DevExpress.XtraGauges.Win.Base.StateImageIndicatorComponent() {Me.StateImageIndicatorComponent1})
        Me.circularGauge1.Name = "circularGauge1"
        Me.circularGauge1.RangeBars.AddRange(New DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleRangeBarComponent() {Me.ArcScaleRangeBarComponent1})
        Me.circularGauge1.Scales.AddRange(New DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent() {Me.ArcScaleComponent1})
        '
        'StateImageIndicatorComponent1
        '
        Me.StateImageIndicatorComponent1.ImageLayoutMode = DevExpress.XtraGauges.Core.Drawing.ImageLayoutMode.BottomRight
        ImageIndicatorState2.Name = "State1"
        Me.StateImageIndicatorComponent1.ImageStateCollection.AddRange(New DevExpress.XtraGauges.Core.Model.ImageIndicatorState() {ImageIndicatorState2})
        Me.StateImageIndicatorComponent1.IndicatorScale = Me.ArcScaleComponent1
        Me.StateImageIndicatorComponent1.Name = "circularGauge1_StateImageIndicator1"
        Me.StateImageIndicatorComponent1.StateImages = Me.ImageList1
        Me.StateImageIndicatorComponent1.StateIndex = 0
        '
        'ArcScaleComponent1
        '
        Me.ArcScaleComponent1.AppearanceMajorTickmark.BorderBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White")
        Me.ArcScaleComponent1.AppearanceMajorTickmark.ContentBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White")
        Me.ArcScaleComponent1.AppearanceMinorTickmark.BorderBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White")
        Me.ArcScaleComponent1.AppearanceMinorTickmark.ContentBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:White")
        Me.ArcScaleComponent1.AppearanceTickmarkText.Font = New System.Drawing.Font("Tahoma", 8.5!)
        Me.ArcScaleComponent1.AppearanceTickmarkText.TextBrush = New DevExpress.XtraGauges.Core.Drawing.SolidBrushObject("Color:#484E5A")
        Me.ArcScaleComponent1.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(125.0!, 125.0!)
        Me.ArcScaleComponent1.EndAngle = 90.0!
        Me.ArcScaleComponent1.MajorTickCount = 0
        Me.ArcScaleComponent1.MajorTickmark.FormatString = "{0:F0}"
        Me.ArcScaleComponent1.MajorTickmark.ShapeOffset = -14.0!
        Me.ArcScaleComponent1.MajorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style16_1
        Me.ArcScaleComponent1.MajorTickmark.TextOrientation = DevExpress.XtraGauges.Core.Model.LabelOrientation.LeftToRight
        Me.ArcScaleComponent1.MaxValue = 100.0!
        Me.ArcScaleComponent1.MinorTickCount = 0
        Me.ArcScaleComponent1.MinorTickmark.ShapeOffset = -7.0!
        Me.ArcScaleComponent1.MinorTickmark.ShapeType = DevExpress.XtraGauges.Core.Model.TickmarkShapeType.Circular_Style16_2
        Me.ArcScaleComponent1.Name = "scale1"
        Me.ArcScaleComponent1.StartAngle = -270.0!
        Me.ArcScaleComponent1.Value = 20.0!
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "78a4211079921cd8604d573ea33f48c3.png")
        Me.ImageList1.Images.SetKeyName(1, "2000px-Warning_sign_font_awesome-red.svg.png")
        '
        'ArcScaleRangeBarComponent1
        '
        Me.ArcScaleRangeBarComponent1.AnchorValue = 1.0!
        Me.ArcScaleRangeBarComponent1.ArcScale = Me.ArcScaleComponent1
        Me.ArcScaleRangeBarComponent1.EndOffset = 5.0!
        Me.ArcScaleRangeBarComponent1.Name = "circularGauge1_RangeBar2"
        Me.ArcScaleRangeBarComponent1.ShowBackground = True
        Me.ArcScaleRangeBarComponent1.StartOffset = 65.0!
        Me.ArcScaleRangeBarComponent1.Value = 10.0!
        Me.ArcScaleRangeBarComponent1.ZOrder = -10
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(204, 152)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(13, 13)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(204, 128)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(13, 13)
        Me.Label9.TabIndex = 25
        Me.Label9.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(204, 80)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(13, 13)
        Me.Label4.TabIndex = 25
        Me.Label4.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(204, 104)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(13, 13)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(248, 154)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(109, 13)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "اجمالي حموله السياره"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(248, 128)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(107, 13)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "اقصي حموله للسياره "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(287, 80)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "حموله الطلبيه"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(277, 104)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(81, 13)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "ح.س خلال اليوم"
        '
        'DelivaryCarComboBox
        '
        Me.DelivaryCarComboBox.FormattingEnabled = True
        Me.DelivaryCarComboBox.Location = New System.Drawing.Point(183, 18)
        Me.DelivaryCarComboBox.Name = "DelivaryCarComboBox"
        Me.DelivaryCarComboBox.Size = New System.Drawing.Size(123, 21)
        Me.DelivaryCarComboBox.TabIndex = 13
        '
        'DelivaryDriverComboBox
        '
        Me.DelivaryDriverComboBox.FormattingEnabled = True
        Me.DelivaryDriverComboBox.Location = New System.Drawing.Point(183, 44)
        Me.DelivaryDriverComboBox.Name = "DelivaryDriverComboBox"
        Me.DelivaryDriverComboBox.Size = New System.Drawing.Size(124, 21)
        Me.DelivaryDriverComboBox.TabIndex = 15
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(183, 76)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(174, 20)
        Me.TextBox3.TabIndex = 26
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(183, 150)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(174, 20)
        Me.TextBox6.TabIndex = 29
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(183, 124)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(174, 20)
        Me.TextBox5.TabIndex = 28
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(183, 100)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(174, 20)
        Me.TextBox4.TabIndex = 27
        '
        'GroupCompoBox
        '
        Me.GroupCompoBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupCompoBox.FormattingEnabled = True
        Me.GroupCompoBox.Location = New System.Drawing.Point(385, 375)
        Me.GroupCompoBox.Name = "GroupCompoBox"
        Me.GroupCompoBox.Size = New System.Drawing.Size(143, 21)
        Me.GroupCompoBox.TabIndex = 43
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton3.Location = New System.Drawing.Point(385, 345)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(222, 24)
        Me.SimpleButton3.TabIndex = 26
        Me.SimpleButton3.Text = "تغيير الفئة"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.SimpleButton4.Location = New System.Drawing.Point(370, 3)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(222, 24)
        Me.SimpleButton4.TabIndex = 26
        Me.SimpleButton4.Text = "تغيير العميل " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'EditOrder
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(622, 455)
        Me.Controls.Add(Me.GroupCompoBox)
        Me.Controls.Add(Label7)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.SimpleButton3)
        Me.Controls.Add(Me.SimpleButton4)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(OrderProductLabel)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.OrderProductComboBox)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(OrderQuntityLabel)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.OrderQuntityTextBox)
        Me.Controls.Add(Me.NewButton)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.OrderClintComboBox)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.OrderIDTextBox)
        Me.Controls.Add(OrderClintLabel)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.ProductLineTextBox)
        Me.Controls.Add(OrderDateLabel)
        Me.Controls.Add(Me.OrderDateDateTimePicker)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "EditOrder"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "تعديل بيانات الطلبيه"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.circularGauge1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateImageIndicatorComponent1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ArcScaleComponent1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ArcScaleRangeBarComponent1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents OrderIDTextBox As TextBox
    Friend WithEvents ProductLineTextBox As TextBox
    Friend WithEvents OrderDateDateTimePicker As DateTimePicker
    Friend WithEvents Button1 As Button
    Friend WithEvents OrderClintComboBox As ComboBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents DeliveryDateDateTimePicker As DateTimePicker
    Friend WithEvents ProductionDateDateTimePicker As DateTimePicker
    Friend WithEvents NewButton As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents OrderQuntityTextBox As TextBox
    Friend WithEvents OrderProductComboBox As ComboBox
    Friend WithEvents Button2 As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents DelivaryCarComboBox As ComboBox
    Friend WithEvents DelivaryDriverComboBox As ComboBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents GroupCompoBox As ComboBox
    Friend WithEvents GaugeControl1 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents circularGauge1 As DevExpress.XtraGauges.Win.Gauges.Circular.CircularGauge
    Private WithEvents StateImageIndicatorComponent1 As DevExpress.XtraGauges.Win.Base.StateImageIndicatorComponent
    Private WithEvents ArcScaleComponent1 As DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleComponent
    Friend WithEvents ImageList1 As ImageList
    Private WithEvents ArcScaleRangeBarComponent1 As DevExpress.XtraGauges.Win.Gauges.Circular.ArcScaleRangeBarComponent
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
End Class
