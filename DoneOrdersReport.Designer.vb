﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class DoneOrdersReport
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.SearchByClint = New DevExpress.XtraEditors.CheckButton()
        Me.SearchByDate = New DevExpress.XtraEditors.CheckButton()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.SearchByCar = New DevExpress.XtraEditors.CheckButton()
        Me.SearchByGroup = New DevExpress.XtraEditors.CheckButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.CarFilterType = New DevExpress.XtraEditors.RadioGroup()
        Me.CheckedComboBoxEdit2 = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.SliderButton = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ClintFilterType = New DevExpress.XtraEditors.RadioGroup()
        Me.CheckedComboBoxEdit1 = New DevExpress.XtraEditors.CheckedComboBoxEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.ToggleSwitch1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.PrintButton = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.CarFilterType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckedComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ClintFilterType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckedComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'DateEdit1
        '
        Me.DateEdit1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(115, 40)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit1.TabIndex = 2
        '
        'DateEdit2
        '
        Me.DateEdit2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(9, 40)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit2.TabIndex = 2
        '
        'SearchByClint
        '
        Me.SearchByClint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SearchByClint.Location = New System.Drawing.Point(9, 13)
        Me.SearchByClint.Name = "SearchByClint"
        Me.SearchByClint.Size = New System.Drawing.Size(268, 23)
        Me.SearchByClint.TabIndex = 3
        Me.SearchByClint.Text = "العميل"
        '
        'SearchByDate
        '
        Me.SearchByDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SearchByDate.Location = New System.Drawing.Point(9, 11)
        Me.SearchByDate.Name = "SearchByDate"
        Me.SearchByDate.Size = New System.Drawing.Size(206, 23)
        Me.SearchByDate.TabIndex = 4
        Me.SearchByDate.Text = "التاريخ"
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LookUpEdit1.Location = New System.Drawing.Point(9, 95)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Size = New System.Drawing.Size(206, 20)
        Me.LookUpEdit1.TabIndex = 5
        '
        'SearchByCar
        '
        Me.SearchByCar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SearchByCar.Location = New System.Drawing.Point(4, 11)
        Me.SearchByCar.Name = "SearchByCar"
        Me.SearchByCar.Size = New System.Drawing.Size(283, 23)
        Me.SearchByCar.TabIndex = 6
        Me.SearchByCar.Text = "السيارة"
        '
        'SearchByGroup
        '
        Me.SearchByGroup.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SearchByGroup.Location = New System.Drawing.Point(9, 66)
        Me.SearchByGroup.Name = "SearchByGroup"
        Me.SearchByGroup.Size = New System.Drawing.Size(206, 23)
        Me.SearchByGroup.TabIndex = 7
        Me.SearchByGroup.Text = "الفئه"
        '
        'GroupControl1
        '
        Me.GroupControl1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupControl1.Controls.Add(Me.GroupBox3)
        Me.GroupControl1.Controls.Add(Me.GroupBox2)
        Me.GroupControl1.Controls.Add(Me.GroupBox1)
        Me.GroupControl1.CustomHeaderButtonsLocation = DevExpress.Utils.GroupElementLocation.AfterText
        Me.GroupControl1.Location = New System.Drawing.Point(343, 4)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(829, 166)
        Me.GroupControl1.TabIndex = 8
        Me.GroupControl1.Text = "فلاتر البحث"
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.SearchByGroup)
        Me.GroupBox3.Controls.Add(Me.SearchByDate)
        Me.GroupBox3.Controls.Add(Me.DateEdit1)
        Me.GroupBox3.Controls.Add(Me.DateEdit2)
        Me.GroupBox3.Controls.Add(Me.LookUpEdit1)
        Me.GroupBox3.Location = New System.Drawing.Point(5, 32)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(224, 125)
        Me.GroupBox3.TabIndex = 12
        Me.GroupBox3.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.CarFilterType)
        Me.GroupBox2.Controls.Add(Me.CheckedComboBoxEdit2)
        Me.GroupBox2.Controls.Add(Me.SearchByCar)
        Me.GroupBox2.Location = New System.Drawing.Point(236, 32)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(294, 125)
        Me.GroupBox2.TabIndex = 11
        Me.GroupBox2.TabStop = False
        '
        'CarFilterType
        '
        Me.CarFilterType.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CarFilterType.Location = New System.Drawing.Point(4, 40)
        Me.CarFilterType.Name = "CarFilterType"
        Me.CarFilterType.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.CarFilterType.Properties.Appearance.Options.UseBackColor = True
        Me.CarFilterType.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "يحتوي علي"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "لا يحتوي علي")})
        Me.CarFilterType.Size = New System.Drawing.Size(283, 29)
        Me.CarFilterType.TabIndex = 9
        '
        'CheckedComboBoxEdit2
        '
        Me.CheckedComboBoxEdit2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckedComboBoxEdit2.Location = New System.Drawing.Point(4, 95)
        Me.CheckedComboBoxEdit2.Name = "CheckedComboBoxEdit2"
        Me.CheckedComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.CheckedComboBoxEdit2.Properties.IncrementalSearch = True
        Me.CheckedComboBoxEdit2.Size = New System.Drawing.Size(283, 20)
        Me.CheckedComboBoxEdit2.TabIndex = 8
        '
        'SliderButton
        '
        Me.SliderButton.Appearance.ForeColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.SliderButton.Appearance.Options.UseForeColor = True
        Me.SliderButton.ImageOptions.Image = Global.OrderManger.My.Resources.Resources.movedown_16x16
        Me.SliderButton.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.MiddleCenter
        Me.SliderButton.Location = New System.Drawing.Point(310, 2)
        Me.SliderButton.Name = "SliderButton"
        Me.SliderButton.Size = New System.Drawing.Size(31, 22)
        Me.SliderButton.TabIndex = 13
        Me.SliderButton.Text = "Down"
        Me.SliderButton.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.ClintFilterType)
        Me.GroupBox1.Controls.Add(Me.CheckedComboBoxEdit1)
        Me.GroupBox1.Controls.Add(Me.SearchByClint)
        Me.GroupBox1.Location = New System.Drawing.Point(534, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(290, 126)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        '
        'ClintFilterType
        '
        Me.ClintFilterType.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ClintFilterType.Location = New System.Drawing.Point(9, 42)
        Me.ClintFilterType.Name = "ClintFilterType"
        Me.ClintFilterType.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.ClintFilterType.Properties.Appearance.Options.UseBackColor = True
        Me.ClintFilterType.Properties.Items.AddRange(New DevExpress.XtraEditors.Controls.RadioGroupItem() {New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "يحتوي علي"), New DevExpress.XtraEditors.Controls.RadioGroupItem(Nothing, "لا يحتوي علي")})
        Me.ClintFilterType.Size = New System.Drawing.Size(268, 27)
        Me.ClintFilterType.TabIndex = 9
        '
        'CheckedComboBoxEdit1
        '
        Me.CheckedComboBoxEdit1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckedComboBoxEdit1.Location = New System.Drawing.Point(9, 97)
        Me.CheckedComboBoxEdit1.Name = "CheckedComboBoxEdit1"
        Me.CheckedComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.CheckedComboBoxEdit1.Properties.IncrementalSearch = True
        Me.CheckedComboBoxEdit1.Size = New System.Drawing.Size(268, 20)
        Me.CheckedComboBoxEdit1.TabIndex = 8
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(196, 4)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(108, 23)
        Me.SimpleButton1.TabIndex = 9
        Me.SimpleButton1.Text = "بحث"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(2, 2)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(1168, 437)
        Me.GridControl1.TabIndex = 10
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.HeaderPanel.Options.UseTextOptions = True
        Me.GridView1.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.GridView1.AppearancePrint.HeaderPanel.Options.UseTextOptions = True
        Me.GridView1.AppearancePrint.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.None
        Me.GridView1.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.GridView1.ColumnPanelRowHeight = 50
        Me.GridView1.FooterPanelHeight = 40
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.AllowCellMerge = True
        Me.GridView1.OptionsView.AllowHtmlDrawHeaders = True
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsView.EnableAppearanceEvenRow = True
        Me.GridView1.OptionsView.EnableAppearanceOddRow = True
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.OptionsView.ShowGroupedColumns = True
        Me.GridView1.OptionsView.ShowGroupPanelColumnsAsSingleRow = True
        Me.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsView.WaitAnimationOptions = DevExpress.XtraEditors.WaitAnimationOptions.Indicator
        '
        'ToggleSwitch1
        '
        Me.ToggleSwitch1.Location = New System.Drawing.Point(81, 3)
        Me.ToggleSwitch1.Name = "ToggleSwitch1"
        Me.ToggleSwitch1.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.[Default]
        Me.ToggleSwitch1.Properties.OffText = "بالطول"
        Me.ToggleSwitch1.Properties.OnText = "بالعرض"
        Me.ToggleSwitch1.Size = New System.Drawing.Size(109, 24)
        Me.ToggleSwitch1.TabIndex = 11
        Me.ToggleSwitch1.ToolTip = "هذا الاختيار يتيح لك اختيار طريقعه عرض اصناف الطلبه اما بالطول او بالعرض"
        Me.ToggleSwitch1.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Information
        '
        'PrintButton
        '
        Me.PrintButton.Location = New System.Drawing.Point(5, 4)
        Me.PrintButton.Name = "PrintButton"
        Me.PrintButton.Size = New System.Drawing.Size(70, 23)
        Me.PrintButton.TabIndex = 13
        Me.PrintButton.Text = "طباعة"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.PrintButton)
        Me.PanelControl1.Controls.Add(Me.ToggleSwitch1)
        Me.PanelControl1.Controls.Add(Me.SliderButton)
        Me.PanelControl1.Controls.Add(Me.GroupControl1)
        Me.PanelControl1.Controls.Add(Me.SimpleButton1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1172, 29)
        Me.PanelControl1.TabIndex = 13
        '
        'PanelControl2
        '
        Me.PanelControl2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelControl2.Controls.Add(Me.GridControl1)
        Me.PanelControl2.Location = New System.Drawing.Point(0, 33)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(1172, 441)
        Me.PanelControl2.TabIndex = 14
        '
        'DoneOrdersReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1172, 474)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.PanelControl2)
        Me.Name = "DoneOrdersReport"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Text = "تقرير مبيعات الطلبيات"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.CarFilterType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckedComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.ClintFilterType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckedComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents SearchByClint As DevExpress.XtraEditors.CheckButton
    Friend WithEvents SearchByDate As DevExpress.XtraEditors.CheckButton
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents SearchByCar As DevExpress.XtraEditors.CheckButton
    Friend WithEvents SearchByGroup As DevExpress.XtraEditors.CheckButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents ToggleSwitch1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents PrintButton As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckedComboBoxEdit1 As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents ClintFilterType As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents CarFilterType As DevExpress.XtraEditors.RadioGroup
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents CheckedComboBoxEdit2 As DevExpress.XtraEditors.CheckedComboBoxEdit
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents SliderButton As DevExpress.XtraEditors.SimpleButton
End Class
