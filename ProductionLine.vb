﻿Imports System.Data.SqlClient

Public Class ProductionLine
    Private Sub ProductionLine_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            Load_PLine()
            DataGridView1.DataSource = PLineDT
            DataGridView1.Columns(0).HeaderText = "خط الانتاج "
            DataGridView1.Columns(1).HeaderText = "السعه الانتاجيه"
            Me.Visible = False

        Catch ex As Exception

        End Try

    End Sub

    Private Sub ToolStripButton1_Click(sender As Object, e As EventArgs) Handles ToolStripButton1.Click
        Try




            If LineNameTextBox.Text <> Nothing Then

                Dim CHKPLineDA As New SqlDataAdapter
                Dim CHKPLineDT As New DataTable
                CHKPLineDA = New SqlDataAdapter("select * from productionline Where LineName Like '" & LineNameTextBox.Text & "' ", SQlManeCon)
                CHKPLineDT.Clear()
                CHKPLineDA.Fill(CHKPLineDT)
                If CHKPLineDT.Rows.Count = 0 Then
                    PLineDT.Rows.Add()
                    Dim last As Integer = PLineDT.Rows.Count - 1
                    PLineDT.Rows(last).Item("linename") = LineNameTextBox.Text
                    PLineDT.Rows(last).Item("linecapacity") = LineCapacityTextBox.Text

                    Dim save As New SqlCommandBuilder(PLineDA)
                    PLineDA.Update(PLineDT)
                    PLineDT.AcceptChanges()
                    Load_PLine()

                ElseIf CHKPLineDT.Rows.Count > 0 Then
                    Dim pos As Integer = BindingContext(PLineDT).Position
                    PLineDT.Rows(pos).Item("linename") = LineNameTextBox.Text
                    PLineDT.Rows(pos).Item("linecapacity") = LineCapacityTextBox.Text
                    Dim save As New SqlCommandBuilder(PLineDA)
                    PLineDA.Update(PLineDT)
                    PLineDT.AcceptChanges()
                    Load_PLine()
                End If
            End If
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub

    Private Sub ToolStripButton2_Click(sender As Object, e As EventArgs) Handles ToolStripButton2.Click
        Try
            Dim pos As Integer = BindingContext(PLineDT).Position
            PLineDT.Rows(pos).Delete()
            Dim save As New SqlCommandBuilder(PLineDA)
            PLineDA.Update(PLineDT)
            PLineDT.AcceptChanges()
            Load_PLine()
        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try



    End Sub

    Private Sub ToolStripButton3_Click(sender As Object, e As EventArgs) Handles ToolStripButton3.Click
        LineNameTextBox.Text = ""
        LineCapacityTextBox.Text = 0

    End Sub


    Private Sub DataGridView1_SelectionChanged(sender As Object, e As EventArgs) Handles DataGridView1.SelectionChanged
        Try
            Dim pos As Integer = BindingContext(PLineDT).Position

            LineNameTextBox.Text = PLineDT.Rows(pos).Item("linename")
            LineCapacityTextBox.Text = PLineDT.Rows(pos).Item("linecapacity")

        Catch ex As Exception

        End Try
    End Sub

    Private Sub LineCapacityTextBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles LineCapacityTextBox.KeyPress
        Try
            If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
                e.Handled = True
                MsgBox("هذا الحقل يقبل ارقام فقط")
            Else

            End If


        Catch ex As Exception
            log_error(Me.Text, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message)
            'MsgBox("لقد حدث خطأً ما " & ex.Message & "  تم حفظ الخطأ في ملف سجل الاخطاء الموجود في   " & Application.StartupPath & "/Log.txt  ")

        End Try
    End Sub
End Class