﻿Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraPrinting
Imports System.Drawing
Imports System.Threading
Imports DevExpress.XtraEditors.Controls

Public Class DoneOrdersReport
    Dim Grid1HLayoutFile As String = "" & Application.StartupPath & "\frmDone_Orders_Grid1_HLayout.xml"
    Dim Grid1VLayoutFile As String = "" & Application.StartupPath & "\frmDone_Orders_Grid1_VLayout.xml"
    Dim GridLayoutType As String


    Private Sub LoadClints()
        Dim da As New SqlDataAdapter("SELECT CusNameAr FROM SL_Customer", SQLCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)
        'With SearchLookUpEdit1.Properties
        '    .DataSource = dt
        '    .DisplayMember = "CusNameAr"
        '    .View.PopulateColumns(dt)
        '    .View.Columns("CusNameAr").Caption = "اسم العميل"

        'End With

        With CheckedComboBoxEdit1.Properties
            .DataSource = dt
            .DisplayMember = "CusNameAr"



        End With
    End Sub
    Private Sub loadCars()
        Dim da As New SqlDataAdapter("SELECT PlatNB FROM Cars", SQlManeCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)

        With CheckedComboBoxEdit2.Properties
            .DataSource = dt
            .DisplayMember = "PlatNB"
            '.PopulateColumns()
            '.Columns("PlatNB").Caption = "السياره"
        End With
    End Sub
    Private Sub LoadGroups()
        Dim da As New SqlDataAdapter("SELECT  GroupName FROM OrdersGroup", SQlManeCon)
        Dim dt As New DataTable
        dt.Clear()
        da.Fill(dt)

        With LookUpEdit1.Properties
            .DataSource = dt
            .DisplayMember = "GroupName"
            .PopulateColumns()
            .Columns("GroupName").Caption = "الفئه"
        End With
    End Sub
    Private Function GetData() As DataTable
        Dim FilterClint, FilterDate, FilterCar, FilterGroup As String
        Dim FilterClintType, FilterCarType As String
        Dim SelectedClints, SelectedCars As String

        Select Case ClintFilterType.SelectedIndex
            Case 0
                FilterClintType = "IN"
            Case 1
                FilterClintType = "NOT IN"
        End Select

        Select Case CarFilterType.SelectedIndex
            Case 0
                FilterCarType = "IN"
            Case 1
                FilterCarType = "NOT IN"
        End Select

        SelectedClints = "('' "
        For Each item As CheckedListBoxItem In CheckedComboBoxEdit1.Properties.Items

            If item.CheckState = CheckState.Checked Then
                SelectedClints = SelectedClints & ",'" & item.ToString & "'"
            End If
        Next
        SelectedClints = SelectedClints & ")"

        SelectedCars = "('' "
        For Each item As CheckedListBoxItem In CheckedComboBoxEdit2.Properties.Items

            If item.CheckState = CheckState.Checked Then
                SelectedCars = SelectedCars & ",'" & item.ToString & "'"
            End If
        Next
        SelectedCars = SelectedCars & ")"




        If SearchByClint.Checked = True Then
            FilterClint = "and OrderClint  " & FilterClintType & SelectedClints & " "
        Else
            FilterClint = ""
        End If
        If SearchByDate.Checked = True Then
            FilterDate = "and DeliveryDate  between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00'  and '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59.59'    "
        Else
            FilterDate = ""
        End If
        If SearchByCar.Checked = True Then
            FilterCar = "and DeliveryCar " & FilterCarType & SelectedCars & " "
        Else
            FilterCar = ""
        End If
        If SearchByGroup.Checked = True Then
            FilterGroup = "and OrderGroup =N'" & LookUpEdit1.Text & "'  "
        Else
            FilterGroup = ""
        End If


        Dim da As New SqlDataAdapter("SELECT
       DoneOrders.OrderID
      ,DoneOrders.OrderClint
      ,DoneOrders.DeliveryCar
      ,DoneOrders.DeliveryDriver
      ,DoneOrders.OrderDate
      ,DoneOrders.DeliveryDate
      ,DoneOrders.TrueDeliveryDate
      ,DoneOrders.InvoiceCode
      ,DoneOrders.OrderGroup
      ,DoneOrders.OrderPrice
      ,DoneOrdersDetails.OrderProduct
      ,DoneOrdersDetails.OrderProduct_ID
      ,DoneOrdersDetails.OrderQuntity
      ,DoneOrdersDetails.TrueOrderQuntity
      ,DoneOrdersDetails.TrueOrderQuntityInTon
      ,DoneOrdersDetails.TotalPrice
  FROM (DoneOrdersDetails join DoneOrders on DoneOrders.OrderID = DoneOrdersDetails.OrderID
 ) where  DoneOrders.OrderID > 0  " & FilterClint & "  " & FilterDate & "  " & FilterCar & "  " & FilterGroup & "
 ", SQlManeCon)
        Dim dt As New DataTable
        da.Fill(dt)
        Return dt
    End Function
    Private Sub ArrangeGrid()
        With GridView1
            .Columns("OrderID").Caption = "كود الطلبيه"
            .Columns("OrderID").Fixed = FixedStyle.Left
            .Columns("OrderClint").Caption = "العميل"
            .Columns("OrderClint").Fixed = FixedStyle.Left
            .Columns("OrderDate").Caption = "تاريخ الطلب"
            .Columns("TrueDeliveryDate").Caption = "تاريخ انشاء الفاتوره"
            .Columns("InvoiceCode").Caption = "رقم الفاتوره"
        End With
        If GridLayoutType = "H" Then
            With GridView1
                .Columns("DeliveryCar").Caption = "السياره"
                .Columns("DeliveryDriver").Caption = "السائق"
                .Columns("DeliveryDate").Caption = "تاريخ التسليم"
                .Columns("OrderGroup").Caption = "الفئه"
                .Columns("OrderPrice").Caption = "اجمالي السعر"
                .Columns("OrderProduct").Caption = "الصنف"
                .Columns("OrderProduct_ID").Caption = "كود الصنف"
                .Columns("OrderQuntity").Caption = "الكميه المطلوبه"
                .Columns("TrueOrderQuntity").Caption = "الكميه الحقيقيه"
                .Columns("TrueOrderQuntityInTon").Caption = "الكميه الحقيقيه بالطن"
                .Columns("TotalPrice").Caption = "السعر"

            End With
        End If
        If GridLayoutType = "V" Then


            For i As Integer = 4 To GridView1.Columns.Count - 1
                GridView1.Columns(i).Summary.Add(DevExpress.Data.SummaryItemType.Sum, GridView1.Columns(i).FieldName.ToString, "{0:n0}")
            Next

            GridView1.Columns("تكلفه الطلبيه").Fixed = FixedStyle.Right
            GridView1.Columns("ا.ح.طن").Fixed = FixedStyle.Right
        End If
        For i As Int32 = 0 To GridView1.Columns.Count - 1

            GridView1.Columns(i).BestFit()
            GridView1.Columns(i).AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center
            GridView1.Columns(i).AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center
        Next
        GridView1.Appearance.FooterPanel.TextOptions.HAlignment = HorzAlignment.Center
        GridView1.Appearance.FooterPanel.Options.UseTextOptions = True

    End Sub
    Private Sub DoneOrdersReport_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadClints()
        loadCars()
        LoadGroups()

    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click, ToggleSwitch1.Toggled
        GridLayoutType = ""
        GridControl1.DataSource = Nothing
        GridView1.Columns.Clear()


        If ToggleSwitch1.IsOn = True Then
            GridLayoutType = "V"
            GridControl1.DataSource = VLayoutView()
            ArrangeGrid()
            If File.Exists(Grid1VLayoutFile) Then
                GridControl1.MainView.RestoreLayoutFromXml(Grid1VLayoutFile)
            End If
        Else

            GridLayoutType = "H"
            GridControl1.DataSource = GetData()
            ArrangeGrid()
            If File.Exists(Grid1HLayoutFile) Then
                GridControl1.MainView.RestoreLayoutFromXml(Grid1HLayoutFile)
            End If
        End If







        GridView1.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        GridView1.AppearancePrint.HeaderPanel.Options.UseTextOptions = True
        GridView1.OptionsPrint.AutoWidth = True
        GridView1.AppearancePrint.FooterPanel.Font = New Font("Times New Roman", 8)
        GridView1.AppearancePrint.FooterPanel.TextOptions.WordWrap = WordWrap.Wrap
        GridView1.AppearancePrint.FooterPanel.Options.UseTextOptions = True
        GridView1.AppearancePrint.FooterPanel.TextOptions.HAlignment = VertAlignment.Center

    End Sub
    Private Sub FrmClosing(sender As Object, e As EventArgs) Handles Me.FormClosing, PrintButton.Click
        If GridLayoutType = "H" Then
            GridControl1.MainView.SaveLayoutToXml(Grid1HLayoutFile)
        End If
        If GridLayoutType = "V" Then
            GridControl1.MainView.SaveLayoutToXml(Grid1VLayoutFile)
        End If
    End Sub
    Private Function VLayoutView() As DataTable
        Dim MainDT As New DataTable

        MainDT = GetData()

        Dim dt As New DataTable

        dt.Columns.Add("OrderID")
        dt.Columns.Add("OrderClint")
        dt.Columns.Add("OrderDate")
        dt.Columns.Add("TrueDeliveryDate")
        dt.Columns.Add("InvoiceCode")

        ProductDA = New SqlDataAdapter("SELECT ProductName FROM Product order by sort  ", SQlManeCon)
        ProductDT.Clear()
        ProductDA.Fill(ProductDT)
        For i As Integer = 0 To ProductDT.Rows.Count - 1
            dt.Columns.Add(ProductDT.Rows(i).Item("ProductName").ToString & " ك.م", GetType(Double))
            dt.Columns.Add(ProductDT.Rows(i).Item("ProductName").ToString & " ك.ح", GetType(Double))
        Next
        dt.Columns.Add("ا.ح.طن")
        dt.Columns.Add("تكلفه الطلبيه")
        Dim myrow() As Data.DataRow
        For i As Integer = 0 To MainDT.Rows.Count - 1
            myrow = dt.Select(" OrderID = '" & MainDT.Rows(i).Item("OrderID") & "'")
            If myrow.Count = 0 Then
                dt.Rows.Add()
                Dim last As Integer = dt.Rows.Count - 1
                dt.Rows(last).Item("OrderID") = MainDT.Rows(i).Item("OrderID")
                dt.Rows(last).Item("OrderClint") = MainDT.Rows(i).Item("OrderClint")
                dt.Rows(last).Item("OrderDate") = MainDT.Rows(i).Item("OrderDate")
                dt.Rows(last).Item("TrueDeliveryDate") = MainDT.Rows(i).Item("TrueDeliveryDate")
                dt.Rows(last).Item("InvoiceCode") = MainDT.Rows(i).Item("InvoiceCode")
                dt.Rows(last).Item("تكلفه الطلبيه") = MainDT.Rows(i).Item("OrderPrice")
                dt.Rows(last).Item(MainDT.Rows(i).Item("OrderProduct") & " ك.م") = MainDT.Rows(i).Item("OrderQuntity")
                dt.Rows(last).Item(MainDT.Rows(i).Item("OrderProduct") & " ك.ح") = MainDT.Rows(i).Item("TrueOrderQuntity")
                dt.Rows(last).Item("ا.ح.طن") = MainDT.Rows(i).Item("TrueOrderQuntityInTon")
            End If
            If myrow.Count <> 0 Then
                myrow(0)(MainDT.Rows(i).Item("OrderProduct") & " ك.م") = MainDT.Rows(i).Item("OrderQuntity")
                myrow(0)(MainDT.Rows(i).Item("OrderProduct") & " ك.ح") = MainDT.Rows(i).Item("TrueOrderQuntity")
                myrow(0)("ا.ح.طن") += MainDT.Rows(i).Item("TrueOrderQuntityInTon")
            End If
        Next




        Return dt
    End Function

    Private Sub PrintButton_Click(sender As Object, e As EventArgs) Handles PrintButton.Click
        GridView1.ShowPrintPreview()

    End Sub

    Private Sub GridView1_PrintInitialize(sender As Object, e As DevExpress.XtraGrid.Views.Base.PrintInitializeEventArgs) Handles GridView1.PrintInitialize
        GridView1.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Dim ps As PrintingSystemBase = TryCast(e.PrintingSystem, PrintingSystemBase)
        ps.PageSettings.PaperKind = Printing.PaperKind.A4
        ps.PageSettings.Landscape = True
        ps.PageSettings.LeftMargin = 5
        ps.PageSettings.RightMargin = 5
        ps.PageSettings.TopMargin = 5
        ps.PageSettings.BottomMargin = 5

    End Sub


    Private Sub SlideUp()


        Do
            PanelControl1.Invoke(Sub() PanelControl1.Height -= 10)
            Thread.Sleep(1)
            If PanelControl1.Height <= 60 Then
                Thread.Sleep(PanelControl1.Height / 4)
            End If
        Loop Until PanelControl1.Height <= 30
        PanelControl1.Invoke(Sub() SliderButton.ImageOptions.Image = OrderManger.My.Resources.Resources.movedown_16x16)
        PanelControl1.Invoke(Sub() SliderButton.Text = "Down")
        GroupControl1.Invoke(Sub() GroupControl1.Visible = True)
        PanelControl1.Invoke(Sub() PanelControl1.Height = 30)

    End Sub
    Private Sub SlideDown()


        Do
            PanelControl1.Invoke(Sub() PanelControl1.Height += 10)
            Thread.Sleep(1)
            If PanelControl1.Height >= 140 Then
                Thread.Sleep(PanelControl1.Height / 10)
            End If
        Loop Until PanelControl1.Height >= 165
        PanelControl1.Invoke(Sub() SliderButton.ImageOptions.Image = OrderManger.My.Resources.Resources.moveup_16x16)
        PanelControl1.Invoke(Sub() SliderButton.Text = "Up")

    End Sub

    Private Sub SliderButton_Click(sender As Object, e As EventArgs) Handles SliderButton.Click
        Select Case SliderButton.Text
            Case "Up"
                Dim t As New Thread(AddressOf SlideUp)
                t.Start()
            Case "Down"
                Dim t As New Thread(AddressOf SlideDown)
                t.Start()
        End Select

    End Sub

    Private Sub GridControl1_MouseEnter(sender As Object, e As EventArgs) Handles GridControl1.MouseEnter
        Select Case SliderButton.Text
            Case "Up"
                Dim t As New Thread(AddressOf SlideUp)
                t.Start()
        End Select
    End Sub
    Private Sub GroupControl1_MouseEnter(sender As Object, e As EventArgs) Handles GroupControl1.MouseEnter
        Select Case SliderButton.Text
            Case "Down"
                Dim t As New Thread(AddressOf SlideDown)
                t.Start()
        End Select
    End Sub
End Class