﻿
Public Class BackUpSetting
    Private Sub BackUpSetting_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim TestConn As New SqlClient.SqlConnection("Data Source=" & My.Settings.Connection & ";Initial Catalog=ERP;Integrated Security=True")
        Try
            TestConn.Open()
            TestConn.Close()
            GroupBox1.Enabled = True
        Catch ex As Exception
            GroupBox1.Enabled = False
            Label5.Text = "لا يمكن تغير هذه الاعدادات من هذا الجهاز"
        End Try
        CheckEdit1.Checked  = My.Settings.DoBackUp
        TextEdit1.Text =      My.Settings.BackUpPath
        SpinEdit1.EditValue = My.Settings.BackUpDuration
        DateEdit1.EditValue = My.Settings.LastBackUp
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        FolderBrowserDialog1.ShowDialog()
        TextEdit1.Text = FolderBrowserDialog1.SelectedPath
    End Sub

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        My.Settings.DoBackUp = CheckEdit1.Checked
        My.Settings.BackUpPath = TextEdit1.Text
        My.Settings.BackUpDuration = SpinEdit1.EditValue
        My.Settings.LastBackUp = DateEdit1.EditValue
        My.Settings.Save()
        MsgBox("تم الحفظ بنجاح")

    End Sub

    Private Sub SimpleButton3_Click(sender As Object, e As EventArgs) Handles SimpleButton3.Click
        HomeScreen.DoBackUp()
        MsgBox("تم اجراء نسخه احتياطيه بنجاح")
    End Sub



    Private Sub TextEdit1_EditValueChanged_1(sender As Object, e As EventArgs) Handles TextEdit1.EditValueChanged

    End Sub

    Private Sub TextEdit1_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles TextEdit1.Validating
        If Not System.IO.Directory.Exists(TextEdit1.Text) Then
            MsgBox("عفوا المسار الذي ادخلته غير صحيح")
            CheckEdit1.Enabled = False
            SimpleButton3.Enabled = False
            SimpleButton2.Enabled = False
        Else
            CheckEdit1.Enabled = True
            SimpleButton3.Enabled = True
            SimpleButton2.Enabled = True
        End If
    End Sub
End Class